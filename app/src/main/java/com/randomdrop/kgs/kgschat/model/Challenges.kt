package com.randomdrop.kgs.kgschat.model

import java.io.DataInputStream


class Challenges {
    // TODO: keep list of current challenges

    fun clear() {

    }

    fun readChallenge(inputStream: DataInputStream) {
        val gameID = inputStream.readInt() // TODO: update challenge if already read

        val roomID = inputStream.readInt()
        val roles = Roles(inputStream)

        // read KProposal
        val gameType = inputStream.readByte().toInt()
        val flags2 = inputStream.readByte()
        val numRoles = inputStream.readByte().toInt()

        if (numRoles > 0) {
            for (i in 1..numRoles) {
                val user = UserList.getUser(inputStream)
                val roleID = inputStream.readByte().toInt()
                if (gameType == 5 && roleID == 4) { // SIMUL_ID && BLACK_ID
                    val handi = inputStream.readByte()
                    val komi = 0.5f * inputStream.readShort()
                }
            }
        }

        val flags = inputStream.readByte()

        // read Rules
        val ruleType = inputStream.readByte()
        val boardSize = inputStream.readByte()
        val handi = inputStream.readByte()
        val komi = 0.5f * inputStream.readShort()
        val timeSystem = inputStream.readByte().toInt()
        if (timeSystem != 0) {
            val mainTime = inputStream.readInt()
            if (timeSystem != 1) {
                val byoyomiTime = inputStream.readInt()
                val auxTime = inputStream.readByte()
            }
        }
    }
}