package com.randomdrop.kgs.kgschat.model

import com.randomdrop.kgs.kgschat.Event
import java.io.DataInputStream
import java.util.*


class UserDetails(val channelID: Int, val user: User) {
    var flags: Int = -1
    lateinit var lastOn: Date

    val forcedNoRank: Boolean
        get() = ((flags and 0x1) != 0)
    val emailWanted: Boolean
        get() = ((flags and 0x2) != 0)
    val emailPrivate: Boolean
        get() = ((flags and 0x4) != 0)

    lateinit var registrationStart: Date
    lateinit var personalName: String
    lateinit var personalEmail: String
    lateinit var personalInfo: String
    lateinit var localeStr: String
    var subscriptions: Subscriptions? = null

    val rankGraph = mutableListOf<Short>()

    fun read(inputStream: DataInputStream) {
        flags = inputStream.readInt()
        lastOn = Date(inputStream.readLong())
        registrationStart = Date(inputStream.readLong())
        personalName = inputStream.readUTF()
        personalEmail = if (emailPrivate) "" else inputStream.readUTF()
        personalInfo = inputStream.readUTF()
        localeStr = inputStream.readUTF()
        if (inputStream.available() > 0) { // NOTE: probably only for admins?
            if (this.emailPrivate) {
                personalEmail = inputStream.readUTF()
            }
            subscriptions = Subscriptions()
            subscriptions?.read(inputStream)
        }
        DetailsUpdateEvent(user.name).emit()
    }

    fun readRankGraph(inputStream: DataInputStream) {
        while (inputStream.available() > 1) {
            rankGraph.add(inputStream.readShort())
        }
    }
}

data class DetailsUpdateEvent(val name: String) {
    companion object : Event<DetailsUpdateEvent>()
    fun emit() = emit(this)
}
