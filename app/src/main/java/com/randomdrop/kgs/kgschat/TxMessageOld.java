package com.randomdrop.kgs.kgschat;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.IOException;
import java.io.OutputStream;

@Deprecated
public class TxMessageOld extends ByteArrayOutputStream implements DataOutput
{
    public static final int HEADER_SIZE = 4;
    public static final int CHANNEL_HEADER_SIZE = 8;
    public static final int MAX_UPLOAD_MESSAGE_SIZE = 8192;

    private TxMessageOld(final short type) {
        this(type, 32);
    }

    public TxMessageOld(final MsgTypesDown type) {
        this((short) type.getId());
    }

    public TxMessageOld(final MsgTypesUp type) {
        this((short) type.getId());
    }

    private TxMessageOld(final short type, final int expectedLength) {
        super(expectedLength);
        this.writeShort(0);
        this.writeShort(type);
    }

    public TxMessageOld(final MsgTypesDown type, final int expectedLength) {
        this((short) type.getId(), expectedLength);
    }

    public TxMessageOld() {
    }

    @Override
    public void writeTo(final OutputStream out) throws IOException {
        final int lcount = this.count - 2;
        this.buf[0] = (byte)(lcount >> 8);
        this.buf[1] = (byte)lcount;
        if (lcount >= 32767) {
            final byte[] len = { 127, -1, (byte)(lcount >> 24), (byte)(lcount >> 16) };
            out.write(len);
        }
        out.write(this.buf, 0, this.count);
    }

    public byte[] closeAndGetBytes() {
        final byte[] result = this.buf;
        this.buf = null;
        return result;
    }

    public byte[] closeAndGetExactBytes() {
        final byte[] result = new byte[this.count];
        System.arraycopy(this.buf, 0, result, 0, this.count);
        this.buf = null;
        return result;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void writeUTF(final String s) {
        int len = 0;
        for (int i = 0; i < s.length(); ++i) {
            final int c = s.charAt(i);
            if (c > 0 && c < 127) {
                ++len;
            }
            else if (c == 0 || c < 2047) {
                len += 2;
            }
            else {
                len += 3;
            }
        }
        this.writeShort(len);
        for (int i = 0; i < s.length(); ++i) {
            final int c = s.charAt(i);
            if (c > 0 && c < 127) {
                this.write(c);
            }
            else if (c == 0 || c < 2047) {
                this.write(0xC0 | c >> 6);
                this.write(0x80 | (0x3F & c));
            }
            else {
                this.write(0xE0 | c >> 12);
                this.write(0x80 | (0x3F & c >> 6));
                this.write(0x80 | (0x3F & c));
            }
        }
    }

    @Override
    public final void writeBoolean(final boolean value) {
        this.write(value ? 1 : 0);
    }

    @Override
    public final void writeByte(final int value) {
        this.write(value);
    }

    @Override
    public final void writeChar(final int value) {
        this.write(value >> 8);
        this.write(value);
    }

    @Override
    public void writeShort(final int value) {
        this.write(value >> 8);
        this.write(value);
    }

    @Override
    public void writeInt(final int value) {
        this.write(value >> 24);
        this.write(value >> 16);
        this.write(value >> 8);
        this.write(value);
    }

    @Override
    public void writeLong(final long value) {
        this.write((int)(value >> 56));
        this.write((int)(value >> 48));
        this.write((int)(value >> 40));
        this.write((int)(value >> 32));
        this.write((int)value >> 24);
        this.write((int)value >> 16);
        this.write((int)value >> 8);
        this.write((int)value);
    }

    @Override
    public void writeFloat(final float value) {
        this.writeInt(Float.floatToIntBits(value));
    }

    @Override
    public void writeDouble(final double value) {
        this.writeLong(Double.doubleToLongBits(value));
    }

    @Override
    public void writeBytes(final String s) {
        for (int i = 0; i < s.length(); ++i) {
            this.write(s.charAt(i));
        }
    }

    @Override
    public void writeChars(final String s) {
        for (int i = 0; i < s.length(); ++i) {
            this.writeChar(s.charAt(i));
        }
    }

    public void copyIn(final DataInputStream in) throws IOException {
        final int amt = in.available();
        while (this.buf.length - this.count < amt) {
            final byte[] newBuf = new byte[this.buf.length * 2];
            System.arraycopy(this.buf, 0, newBuf, 0, this.count);
            this.buf = newBuf;
        }
        this.count += in.read(this.buf, this.count, amt);
    }

    @Override
    public String toString() {
        return "TxMessage[type=" + this.getType() + ", len=" + this.count + "]";
    }

    public short getType() {
        return (short)((this.buf[3] & 0xFF) | this.buf[2] << 8);
    }

    public byte[] getBytes() {
        return this.buf;
    }
}
