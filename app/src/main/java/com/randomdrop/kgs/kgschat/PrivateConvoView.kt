package com.randomdrop.kgs.kgschat

import android.content.Context
import android.view.View
import android.widget.TextView
import com.randomdrop.kgs.kgschat.model.*


class PrivateConvoView(channel: Channel, val users: UserList, context: Context) : ChatBoxView(channel, context) {

    override fun startListening() {
        super.startListening()
        processConvoState(ConvoStateChanged(channel.channelID))
        ConvoStateChanged.on(this::processConvoState)
        processDetails(DetailsUpdateEvent((channel as PrivateConvo).peer.name))
        DetailsUpdateEvent.on(this::processDetails)
    }

    override fun stopListening() {
        super.stopListening()
        ConvoStateChanged.off(this::processConvoState)
        DetailsUpdateEvent.off(this::processDetails)
    }

    override fun setConvoStatus() {
        if (!channel.connected) {
            chatText.text.clear()
            chatText.hint = "Chat Disabled: No Connection"
            isJoin = false
            chatButton.text = context.getString(R.string.send)
            chatText.isEnabled = false
            chatButton.isEnabled = false
        } else {
            val stateMsg = (channel as PrivateConvo).stateMsg
            if (channel.chatsBlocked) {
                chatText.text.clear()
                chatText.hint = "Chat Disabled: $stateMsg"
                chatText.isEnabled = false
                if (channel.isOldChannel) {
                    isJoin = true
                    chatButton.text = context.getString(R.string.join)
                    chatButton.isEnabled = true
                } else {
                    isJoin = false
                    chatButton.text = context.getString(R.string.send)
                    chatButton.isEnabled = false
                }
            } else {
                chatText.hint = "Chat Here $stateMsg"
                chatText.isEnabled = true
                isJoin = false
                chatButton.text = context.getString(R.string.send)
                chatButton.isEnabled = true
            }
        }
    }

    private fun processConvoState(ev: ConvoStateChanged) {
        if (ev.channelID == channel.channelID) {
            post {
                setConvoStatus()
            }
        }
    }

    private fun processDetails(ev: DetailsUpdateEvent) {
        val peer = (channel as PrivateConvo).peer
        if (ev.name == peer.name) {
            val infoView = findViewById<TextView>(R.id.userDetailsText)
            this.users.details.forEach { detail ->
                if (detail.user == peer) {
                    if (detail.flags != -1) {
                        var infoText = peer.name + "\n"
                        if (detail.personalName.isNotEmpty())
                            infoText += "Real name: ${detail.personalName}\n"
                        if (detail.personalEmail.isNotEmpty())
                            infoText += "Email: ${detail.personalEmail}\n"
                        if (detail.localeStr.isNotEmpty())
                            infoText += "Locale: ${detail.localeStr}\n"
                        infoText += "Last on: ${detail.lastOn}\n"
                        if (detail.personalInfo.isNotEmpty())
                            infoText += "Notes: ${detail.personalInfo}"
                        infoView.text = infoText
                    } else {
                        infoView.text = "Empty Info"
                    }
                    return@processDetails
                }
            }
        }
    }

    override fun onPersonButtonClick(view: View, model: Model) {
        val name = (channel as PrivateConvo).peer.name
        Commands.requestDetails(name)
        findViewById<View>(R.id.user_details).visibility = View.VISIBLE
    }

    fun closeDetails() {
        findViewById<View>(R.id.user_details).visibility = View.GONE
    }
}