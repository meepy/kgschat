package com.randomdrop.kgs.kgschat

import android.graphics.Color
import android.graphics.Typeface
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.widget.Toolbar
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.Gravity
import android.view.Menu
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.FrameLayout
import android.widget.TextView
import com.randomdrop.kgs.kgschat.MainActivity.Companion.context
import com.randomdrop.kgs.kgschat.model.*
import kotlinx.android.synthetic.main.activity_main.*


class Navigation(val activity: MainActivity) {

    private lateinit var model: Model

    private var isLoginView = true
    var currentView: View? = null // what's currently selected by the Navigator
        private set(value) {
            field = value
        }
    private lateinit var navMenuButton: BadgeDrawerToggle

    val allChatRooms: MutableMap<Int, ChatBoxView> = HashMap()
    private val menuItemToRoom: MutableMap<Int, Channel> = HashMap()

    fun start(model: Model) {
        this.model = model

        val toolbar = activity.findViewById<Toolbar>(R.id.toolbar)
        val mDrawerLayout : DrawerLayout = activity.findViewById(R.id.drawer_layout)
        val navigationView: NavigationView = activity.findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener local@{ menuItem ->
            val id = menuItem.itemId
            if (id == R.id.item1) {
                setLoginView()
            } else {
                val room = menuItemToRoom[id] ?: return@local false
                setFramedView(room)
            }
            mDrawerLayout.closeDrawers()
            return@local true
        }

        navMenuButton = BadgeDrawerToggle(activity, mDrawerLayout, toolbar,
                R.string.navigator_open, R.string.navigator_close)
        mDrawerLayout.addDrawerListener(navMenuButton)
        navMenuButton.setOnLongClickListener(toolbar, View.OnLongClickListener {
            activity.showClearChatDialog()
            true
        })
        navMenuButton.syncState()

        currentView = activity.findViewById(R.id.content_login)
    }

    private fun setupChatBoxView(channel: Channel): ChatBoxView {
        var view = allChatRooms[channel.channelID]
        if (view == null) {
            view = if (channel is PrivateConvo) {
                PrivateConvoView(channel, model.users, activity)
            } else {
                ChatBoxView(channel, activity)
            }

            view.visibility = GONE
            allChatRooms[channel.channelID] = view
            view.startListening()

            val frame = activity.findViewById<FrameLayout>(R.id.content_view)
            frame.addView(view)
        }
        return view
    }

    fun openDrawer() {
        val mDrawerLayout : DrawerLayout = activity.findViewById(R.id.drawer_layout)
        mDrawerLayout.openDrawer(GravityCompat.START)
    }

    fun setFramedView(room: Channel) {
        isLoginView = false

        val toolbar = activity.findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = room.name

        val channel = allChatRooms[room.channelID]
        currentView?.visibility = GONE
        currentView = channel
        currentView?.visibility = VISIBLE

        if (channel != null && channel.isAtBottom)
            channel.clearUnreadMessages()

        val peopleBadge = activity.findViewById<TextView>(R.id.badge_notification_1)
        if (room is Room) {
            peopleBadge.text = "${room.users.size}"
        } else {
            peopleBadge.text = "?" //"【i】"
        }

        activity.people_button.visibility = VISIBLE
    }

    fun onBackPressed() {
        if (isLoginView) {
            ExitDialog().show(activity.fragmentManager, "ExitDialog")
        } else {
            setLoginView()
        }
    }

    fun stopListening() {
        RoomListEvent.off(this::processRoomList)
        ConvoListEvent.off(this::processConvoList)
        ChannelStatusEvent.off(this::processChannelStatusEvent)

        RoomUsersChangedEvent.off(this::processRoomUsersChangedEvent)

        for (chatView in allChatRooms.values) {
            chatView.stopListening()
        }
    }

    fun startListening() {
        processRoomList(null)
        RoomListEvent.on(this::processRoomList)
        processConvoList(null)
        ConvoListEvent.on(this::processConvoList)

        resetChannelStatuses()
        ChannelStatusEvent.on(this::processChannelStatusEvent)

        if (currentView != null && currentView is ChatBoxView) {
            val channel = (currentView as ChatBoxView).channel
            if (channel is Room)
                processRoomUsersChangedEvent(RoomUsersChangedEvent(channel))
        }
        RoomUsersChangedEvent.on(this::processRoomUsersChangedEvent)

        for (chatView in allChatRooms.values) {
            chatView.startListening()
        }
    }

    private fun processRoomUsersChangedEvent(ev: RoomUsersChangedEvent) {
        activity.runOnUiThread {
            if (currentView != null && currentView is ChatBoxView) {
                val currRoom = (currentView as ChatBoxView).channel
                if (ev.room == currRoom) {
                    val numUsers = (currRoom as Room).users.size
                    val peopleBadge = activity.findViewById<TextView>(R.id.badge_notification_1)
                    peopleBadge.text = "$numUsers"
                }
            }
        }
    }

    private fun updateTotalUnread() {
        var totalUnread = 0
        for ((_, chatView) in allChatRooms) {
            totalUnread += chatView.unreadMessages
        }
        when {
            totalUnread == 0 -> navMenuButton.badgeText = ""
            totalUnread < 100 -> navMenuButton.badgeText = "$totalUnread"
            else -> navMenuButton.badgeText = "!!"
        }
    }

    private fun addRoomtoMenuGroup(menu: Menu, groupID: Int, channel: Channel) {
        val title = SpannableString(channel.name)
        title.setSpan(ForegroundColorSpan(if (channel.connected) Color.BLACK else Color.GRAY),
                0, channel.name.length, 0)

        val item = menu.add(groupID, channel.channelID, 1, title)
        val unreadView = TextView(context)
        unreadView.gravity = Gravity.CENTER_VERTICAL
        unreadView.setTypeface(null, Typeface.BOLD)
        unreadView.setTextColor(Color.RED)

        val chatView = setupChatBoxView(channel)
        if (chatView.unreadMessages > 0) {
            unreadView.text = "${chatView.unreadMessages}"
            updateTotalUnread()
        }

        chatView.onUnreadMessages { _, numUnread ->
            if (numUnread > 0) {
                unreadView.text = "$numUnread"
            } else {
                unreadView.text = ""
            }
            updateTotalUnread()
        }

        item.actionView = unreadView
        menuItemToRoom[channel.channelID] = channel
    }

    private fun resetChannelStatuses() {
        val navView = activity.findViewById<NavigationView>(R.id.nav_view)
        for ((_, chatView) in allChatRooms) {
            changeRoomColor(navView.menu, chatView.channel, chatView.channel.connected)
        }
    }

    private fun processChannelStatusEvent(ev: ChannelStatusEvent) {
        activity.runOnUiThread {
            val navView = activity.findViewById<NavigationView>(R.id.nav_view)
            val channel = menuItemToRoom[ev.channelID]
            if (channel != null)
                changeRoomColor(navView.menu, channel, ev.connected)
        }
    }

    private fun changeRoomColor(menu: Menu, channel: Channel, enabled: Boolean) {
        val newTitle = SpannableString(channel.name)
        newTitle.setSpan(ForegroundColorSpan(if (enabled) Color.BLACK else Color.GRAY),
                0, channel.name.length, 0)
        menu.findItem(channel.channelID)?.title = newTitle
    }

    private fun processRoomList(ev: RoomListEvent?) {
        activity.runOnUiThread {
            val navView = activity.findViewById<NavigationView>(R.id.nav_view)
            val menu = navView.menu
            menu.removeGroup(R.id.room_items)

            model.rooms.eachChannel { channel ->
                addRoomtoMenuGroup(menu, R.id.room_items, channel)
            }

            // NOTE: this shouldn't happen as we are not deleting Rooms
            if (currentView is ChatBoxView) {
                val room = (currentView as ChatBoxView).channel
                if (room is Room && menu.findItem(room.channelID) == null) {
                    setLoginView()
                }
            }
        }
    }

    private fun processConvoList(ev: ConvoListEvent?) {
        activity.runOnUiThread {
            val navView = activity.findViewById<NavigationView>(R.id.nav_view)
            val menu = navView.menu
            menu.removeGroup(R.id.convo_items)

            model.convos.eachConvo { channel ->
                addRoomtoMenuGroup(menu, R.id.convo_items, channel)
            }

            // NOTE: PrivateConvo gets a new channel on peer reconnect
            if (currentView is ChatBoxView) {
                val room = (currentView as ChatBoxView).channel
                if (room is PrivateConvo && menu.findItem(room.channelID) == null) {
                    if (room.newConvo != null)
                        setFramedView(room.newConvo!!)
                    else
                        setLoginView()
                }
            }

            if (ev?.channelID != null && menu.findItem(ev.channelID) != null) {
                allChatRooms[ev.channelID]?.let {
                    setFramedView(it.channel)
                }
            }
        }
    }

    fun setLoginView() {
        isLoginView = true

        val toolbar = activity.findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "KGS Chat"
        currentView?.visibility = GONE
        currentView = activity.findViewById(R.id.content_login)
        currentView?.visibility = VISIBLE

        activity.people_button.visibility = GONE
    }
}