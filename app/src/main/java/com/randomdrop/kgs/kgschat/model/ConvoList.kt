package com.randomdrop.kgs.kgschat.model

import android.util.SparseArray


class ConvoList {
    val allConvos = SparseArray<PrivateConvo>()
    val currentConvo = HashMap<String, PrivateConvo>()

    fun startListening() {
        UserStatusEvent.on(this::processUserStatus)
    }

    fun stopListening() {
        UserStatusEvent.off(this::processUserStatus)
    }

    @Synchronized
    fun put(channelID: Int, convo: PrivateConvo) {
        currentConvo[convo.peer.name]?.let {
            if (it != convo) {
                convo.copy(it)
                it.newConvo = convo
            }
        }
        allConvos.put(channelID, convo)
        currentConvo[convo.peer.name] = convo
    }

    @Synchronized
    fun get(channelID: Int): PrivateConvo? {
        return allConvos[channelID]
    }

    @Synchronized
    fun eachChannel(handler: (channel: Channel) -> Unit) {
        synchronized(this) {
            allConvos.forEach { convo ->
                handler(convo)
            }
        }
    }

    @Synchronized
    fun eachConvo(handler: (channel: Channel) -> Unit) {
        synchronized(this) {
            currentConvo.forEach { (name, convo) ->
                handler(convo)
            }
        }
    }

    @Synchronized
    private fun processUserStatus(ev: UserStatusEvent) {
        currentConvo[ev.name]?.update()
    }
}