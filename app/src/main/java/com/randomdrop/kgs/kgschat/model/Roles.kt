package com.randomdrop.kgs.kgschat.model

import java.io.DataInputStream
import kotlin.experimental.and


class Roles {
    constructor(inputStream: DataInputStream) {
        readRoles(inputStream)
    }

    fun readRoles(inputStream: DataInputStream) {
        while (true) {
            val roleID = inputStream.readByte().toInt()
            if (roleID == -1) {
                val flags = inputStream.readShort()
                if ((flags and 0x100) > 0) { // NAMED_BIT
                    val name = inputStream.readUTF()
                }
                return
            }
            val user = UserList.getUser(inputStream)
        }
    }
}