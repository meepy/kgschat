package com.randomdrop.kgs.kgschat

/* Usage:

data class MyEvent(val arg1: Type1, val arg2: Type2, ...) {
    companion object : Event<MyEvent>()

    fun emit() = emit(this)
}

MyEvent on {
    handle the event
}

MyEvent(some args).emit()

 */
open class Event<T> {
    private var handlers = listOf<(T) -> Unit>()

    @Synchronized
    infix fun on(handler: (T) -> Unit) {
        handlers += handler
    }

    @Synchronized
    infix fun off(handler: (T) -> Unit) {
        handlers -= handler
    }

    @Synchronized
    fun emit(event: T) {
        for (subscriber in handlers) {
            subscriber(event)
        }
    }
}
