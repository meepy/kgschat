package com.randomdrop.kgs.kgschat.model

import android.util.SparseArray
import com.randomdrop.kgs.kgschat.Event
import com.randomdrop.kgs.kgschat.MsgTypesDown
import java.io.DataInputStream
import java.util.*

// extension function for sparsearray
inline fun <V> SparseArray<out V>.forEach(action: (V) -> Unit) {
    for (i in 0 until size())
        action(valueAt(i))
}

class RoomList {

    val rooms = SparseArray<Room>()
    val channels = SparseArray<Room>()
    val categories = HashMap<CategoryType, Int>()

    fun startListening() {
        MsgTypesDown.ROOM_NAMES.on(this::processMsgRoomNames)
        MsgTypesDown.ROOM_JOIN.on(this::processMsgRoomJoin)
        MsgTypesDown.ROOM_CHANNEL_INFO.on(this::processMsgRoomChannelInfo)
        MsgTypesDown.ROOM_DESC.on(this::processMsgRoomDescription)
        MsgTypesDown.USER_ADDED.on(this::processMsgUserAdded)
        MsgTypesDown.USER_REMOVED.on(this::processMsgUserRemoved)
    }

    fun stopListening() {
        MsgTypesDown.ROOM_NAMES.off(this::processMsgRoomNames)
        MsgTypesDown.ROOM_JOIN.off(this::processMsgRoomJoin)
        MsgTypesDown.ROOM_CHANNEL_INFO.off(this::processMsgRoomChannelInfo)
        MsgTypesDown.ROOM_DESC.off(this::processMsgRoomDescription)
        MsgTypesDown.USER_ADDED.off(this::processMsgUserAdded)
        MsgTypesDown.USER_REMOVED.off(this::processMsgUserRemoved)
    }

    @Synchronized
    fun findRoomByName(name: String): Room? {
        rooms.forEach { room ->
            if (room.name == name)
                return room
        }
        return null
    }

    private fun processMsgRoomChannelInfo(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        readRoomChanInfo(inputStream)
    }

    @Synchronized
    private fun processMsgRoomNames(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        while (inputStream.available() > 0) {
            val channelID = inputStream.readInt()
            val name = inputStream.readUTF()
            val flags = inputStream.readShort()

            synchronized(this) {
                var foundRoom = rooms[channelID]

                if (foundRoom == null) {
                    foundRoom = Room(channelID, name, flags)
                    rooms.put(channelID, foundRoom)
                } else {
                    foundRoom.name = name
                    foundRoom.flags = flags
                }
                foundRoom.connectionStatus(true)
            }
        }
        RoomListEvent().emit()
    }

    private fun processMsgRoomDescription(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        rooms[channelID]?.readDescriptionAndOwners(inputStream)
    }

    private fun processMsgRoomJoin(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        rooms[channelID]?.readGamesAndChallenges(inputStream)
    }

    private fun processMsgRoomNameFlush(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val room = rooms[channelID]

        /* TODO:
        if (!channel.isJoined()) {
            channel.name = null;
            channel.fetchName()
        }
        */
    }

    private fun processMsgUserAdded(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        rooms[channelID]?.addUser(inputStream)
    }

    private fun processMsgUserRemoved(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        rooms[channelID]?.removeUser(inputStream)
    }

    fun eachChannel(handler: (channel: Channel) -> Unit) {
        synchronized(this) {
            rooms.forEach { room ->
                handler(room)
            }
        }
    }

    /*
    @Synchronized
    fun getRoomsAndListen(processMsg: (String) -> Unit,
                             listener: (RawMessageEvent) -> Unit) {
        for (msg in messages)
            processMsg(msg)
        RawMessageEvent.on(listener)
    }
    */

    enum class CategoryType {
        MAIN,
        TEMPORARY,
        CLUBS,
        LESSONS,
        TOURNAMENT,
        FRIENDLY,
        NATIONAL,
        SPECIAL;
    }

    fun readCategories(inputStream: DataInputStream) {
        val num = inputStream.readByte()

        for (i in 0 until num) {
            categories[CategoryType.values()[i]] = inputStream.readInt()
        }
    }

    @Synchronized
    fun readRoomChanInfo(inputStream: DataInputStream): Boolean {
        val channelID = inputStream.readInt()
        if (channelID == 0)
            return false

        val category = readCategory(inputStream)

        var room = rooms[channelID]

        if (room == null) { // TODO: what to do with these
            room = Room(channelID, "", 0)
            channels.append(channelID, room)
//            Log.d(TAG, "Unnamed channel info read: $channelID, ${category.name}")
        }

        room.category = category

        return true
    }

    fun readCategory(inputStream: DataInputStream): CategoryType {
        val id = inputStream.readByte().toInt()
        if (id >= 0 && id < CategoryType.SPECIAL.ordinal) {
            return CategoryType.values()[id]
        }
        if (id == CategoryType.SPECIAL.ordinal + 1) {
            return CategoryType.SPECIAL
        }
        throw RuntimeException("Invalid channel category: " + id.toString())
    }
}

class RoomListEvent {
    companion object : Event<RoomListEvent>()

    fun emit() = emit(this)
}
