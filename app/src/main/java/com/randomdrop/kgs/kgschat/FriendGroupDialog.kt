package com.randomdrop.kgs.kgschat

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle
import com.randomdrop.kgs.kgschat.model.FriendGroupTypes

@SuppressLint("ValidFragment")
class FriendGroupDialog() : DialogFragment() {

    interface FriendGroupDialogListener {
        fun onDialogItemClick(dialog: DialogFragment)
    }

    private lateinit var mListener: FriendGroupDialogListener

    lateinit var title: String
    lateinit var itemArray: List<String>
    lateinit var groupType: FriendGroupTypes

    var selected: String? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setTitle(title)
                .setItems(itemArray.toTypedArray()) { dialog, which ->
                    selected = itemArray[which]
                    mListener.onDialogItemClick(this)
                }
                .setPositiveButton(R.string.ok) { dialog, id ->
                }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        try {
            mListener = activity as FriendGroupDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException((activity.toString() +
                    " must implement FriendGroupDialogListener"))
        }
    }
}