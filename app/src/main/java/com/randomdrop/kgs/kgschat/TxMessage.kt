package com.randomdrop.kgs.kgschat

import java.io.ByteArrayOutputStream
import java.io.DataOutput
import java.io.OutputStream
import java.lang.Float.floatToIntBits
import java.lang.Double.doubleToLongBits
import kotlin.experimental.and
import kotlin.experimental.or

class TxMessage : ByteArrayOutputStream, DataOutput {

    constructor()
    constructor(type: Int) : this(type, 32)

    constructor(type: MsgTypesDown) : this(type.id)
    constructor(type:MsgTypesDown, expectedLength: Int) : this(type.id, expectedLength)

    constructor(type:MsgTypesUp) : this(type.id)
    constructor(type:MsgTypesUp, expectedLength: Int) : this(type.id, expectedLength)

    constructor(type: Int, expectedLength: Int) : super(expectedLength) {
        writeShort(0)
        writeShort(type)
    }

    override fun writeDouble(v: Double) {
        this.writeLong(doubleToLongBits(v))
    }

    override fun writeBytes(s: String?) {
        for (c in s.orEmpty()) {
            this.write(c.toInt())
        }
    }

    override fun writeByte(v: Int) {
        this.write(v)
    }

    override fun writeFloat(v: Float) {
        this.writeInt(floatToIntBits(v))
    }

    override fun writeChars(s: String?) {
        for (c in s.orEmpty()) {
            this.writeChar(c.toInt())
        }
    }

    override fun writeChar(v: Int) {
        this.write(v.shr(8))
        this.write(v)
    }

    override fun writeBoolean(v: Boolean) {
        this.write(if (v) 1 else 0)
    }

    override fun writeUTF(s: String?) {
        var len = 0

        fun isOneByte(c: Int): Boolean { return c in 1..126 }
        fun isTwoBytes(c: Int): Boolean { return c == 0 || c < 2047 }

        for(i in 0 until s!!.length){
            val c = s[i].toInt()
            when {
                isOneByte(c) -> ++len
                isTwoBytes(c) -> len += 2
                else -> len += 3
            }
        }

        writeShort(len)
        for(i in 0 until s!!.length) {
            val c = s[i].toInt()
            when {
                isOneByte(c) -> this.write(c)
                isTwoBytes(c) -> {
                    this.write(0xC0 or c.shr(6) )
                    this.write(0x80 or (0x3F and c))
                }
                else -> {
                    this.write(0xE0 or c.shr(12) )
                    this.write(0x80 or (0x3F and c.shr(6)))
                    this.write(0x80 or (0x3F and c))
                }
            }
        }
    }

    override fun writeInt(v: Int) {
        this.write(v.shr(24))
        this.write(v.shr(16))
        this.write(v.shr(8))
        this.write(v)
    }

    override fun writeLong(v: Long) {
        for (i in 56 downTo 8 step 8) {
            this.write(v.shr(i).toInt())
        }
        this.write(v.toInt())
    }

    override fun writeShort(v: Int) {
        this.write(v.shr(8))
        this.write(v)
    }

    override fun writeTo(out: OutputStream?) {
        val lcount = this.count - 2
        this.buf[0] = lcount.shr(8).toByte()
        this.buf[1] = lcount.toByte()
        if (lcount >= 32767) {
            val len: ByteArray = byteArrayOf(
                    127,
                    -1,
                    lcount.shr(24).toByte(),
                    lcount.shr(16).toByte())

            out!!.write(len)
        }

        out!!.write(this.buf, 0, this.count)
    }

    override fun size(): Int {
        return this.count
    }

    override fun toString(): String {
        return "TxMessage[type=" + this.javaClass.name + ", len=" + this.count + "]"
    }

    fun getType(): Short {
        val v3 = this.buf[3].toShort().and(0xFF)
        val v2 =  this.buf[2].toInt().shl(8).toShort()
        return v3.or(v2)
    }

    fun getBytes() : ByteArray {
        return this.buf
    }
}