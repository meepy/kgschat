package com.randomdrop.kgs.kgschat


enum class ClientType {
    JAVAWS,
    STANDALONE,
    APPLET,
    GTP,
    ROOM_MANAGER,
    UNIT_TEST,
    STRESS_TEST,
    ANDROID,
    HTML,
    JSON,
    EXTRA_1,
    EXTRA_2,
    EXTRA_3
}