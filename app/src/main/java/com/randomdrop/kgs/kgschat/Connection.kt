package com.randomdrop.kgs.kgschat

import java.io.*
import java.net.ConnectException
import java.net.NoRouteToHostException
import java.net.Socket
import java.net.UnknownHostException
import java.security.AccessControlException

/*
    The socket thread. This should be some kind of Handler
 */
class Connection : Thread() {

    companion object {
        private const val host = "goserver.gokgs.com"
        private const val port = 2379
        private const val PING_PERIOD = 7000L
    }

    private lateinit var outputStream: DataOutputStream
    private lateinit var inputStream: DataInputStream
    private lateinit var sock: Socket

    private var lastOutboundMsgTime: Long = 0L

    var closing: Boolean = false
        @Synchronized get
        @Synchronized set

    private val pingThread = object : Thread() {
        var inactive = true
            @Synchronized get
            @Synchronized set

        private var didPings = 0

        override fun run() {
            inactive = false

            while (!inactive) {
                val now = System.currentTimeMillis();
                var sleepTime = lastOutboundMsgTime + PING_PERIOD - now
                if (sleepTime <= 0L) {
                    didPings++
                    send(TxMessage(MsgTypesUp.PING))
                    sleepTime = PING_PERIOD
                }
                if (didPings >= 5) {
                    send(TxMessage(MsgTypesUp.WAKE_UP))
                    didPings = 0
                }
                sleep(sleepTime)
            }
        }
    }

    fun stopConnection() {
        closing = true
        pingThread.inactive = true
    }

    override fun run() {
        var error = ""
        try {
            sock = Socket(host, port)
            sock.keepAlive = false
        } catch (e: UnknownHostException) {
            error = "NO HOST " + e.message
        } catch (e: NoRouteToHostException) {
            error = "NO ROUTE " + e.message
        } catch (e: ConnectException) {
            error = "SERVER_DOWN " + e.message
        } catch (e: IOException) {
            error = "UNABLE TO CONNECT " + e.message
        } catch (e: AccessControlException) {
            error = "MUST ALLOW NETWORK " + e.message
        } catch (e: Throwable) {
            error = "ERROR " + e.message
        } finally {
            if (!error.isEmpty()) {
                ConnectionEvent(false, "OPEN_EVENT", error).emit()
                return
            }
            ConnectionEvent(true, "OPEN_EVENT", "").emit()
        }

        try {
            inputStream = DataInputStream(BufferedInputStream(sock.getInputStream()))
            outputStream = DataOutputStream(sock.getOutputStream())
            inputStream.readInt()

            while (!closing) {
                var len = inputStream.readShort().toInt()
                if (len == 32767) {
                    len = inputStream.readInt()
                }
                if (len < 0) {
                    throw IOException("Invalid message length: $len (0x${Integer.toString(len and 0xFFFF, 16)})")
                }
                val buf = ByteArray(len)
                inputStream.readFully(buf)

                ConnectionEvent(true, "MESSAGE_IN_EVENT", buf).emit()
            }
        } catch (e: EOFException) {
            error = "EOFException " + e.message
        } catch (e: IOException) {
            error = "IOException " + e.message
        } catch (e: Throwable) {
            error = "ERROR " + e.message
        } finally {
            pingThread.inactive = true
            if (error.isEmpty())
                error = close()
            else
                error = "$error - " + close()
            ConnectionEvent(false, "CLOSED_EVENT", error).emit()
        }
    }

    private fun close(): String {
        try {
            sock.close()
            inputStream.close()
            outputStream.close()
        } catch (e: Exception) {
            return "ERROR " + e.message
        }

        return "OK"
    }

    @Synchronized
    fun send(tx: ByteArray) {
        try {
            ConnectionEvent(true, "MESSAGE_OUT_EVENT", ": [${tx.size}]").emit()
            val lcount = tx.size - 2
            tx[0] = (lcount shr 8).toByte()
            tx[1] = (lcount).toByte()
            if (lcount >= 32767) {
                val len = byteArrayOf(127, -1, (lcount shr 24).toByte(),
                        (lcount shr 16).toByte())
                outputStream.write(len)
            }
            outputStream.write(tx)
            outputStream.flush()
            lastOutboundMsgTime = System.currentTimeMillis()
        } catch (e: Throwable) {
            val error = "Send error " + e.message + " - " + close()
            ConnectionEvent(false, "CLOSED_EVENT", error).emit()
        }

        if (pingThread.inactive) {
            pingThread.start()
        }
    }

    fun send(tx: TxMessage) {
        send(tx.toByteArray())
    }
}

data class ConnectionEvent(val connected: Boolean, val msg: String, val arg: Any) {
    companion object : Event<ConnectionEvent>()

    fun emit() = emit(this)
}

