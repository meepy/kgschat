package com.randomdrop.kgs.kgschat.model

import java.io.DataInputStream
import java.util.*


class Subscriptions {

    data class Subscription(val start: Long, val end: Long)

    private val current: MutableList<Subscription> = LinkedList()

    fun read(inputStream: DataInputStream) {
        val num = inputStream.readByte().toInt()
        for (i in 1..num) {
            val start = inputStream.readLong()
            val end = inputStream.readLong()
            current.add(Subscription(start, end))
        }
    }
}