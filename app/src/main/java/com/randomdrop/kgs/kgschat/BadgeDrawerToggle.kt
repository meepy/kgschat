package com.randomdrop.kgs.kgschat

import android.app.Activity
import android.content.Context
import android.graphics.*
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.graphics.drawable.DrawerArrowDrawable
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ImageButton
import java.lang.reflect.Field

class BadgeDrawerToggle : ActionBarDrawerToggle {

    private var badgeDrawable: BadgeDrawerArrowDrawable? = null

    var isBadgeEnabled: Boolean
        get() = badgeDrawable!!.isEnabled
        set(enabled) {
            badgeDrawable!!.isEnabled = enabled
        }

    var badgeText: String?
        get() = badgeDrawable!!.text
        set(text) {
            badgeDrawable!!.text = text
        }

    var badgeColor: Int
        get() = badgeDrawable!!.backgroundColor
        set(color) {
            badgeDrawable!!.backgroundColor = color
        }

    var badgeTextColor: Int
        get() = badgeDrawable!!.textColor
        set(color) {
            badgeDrawable!!.textColor = color
        }

    private val themedContext: Context?
        get() {
            try {
                val mActivityImplField = ActionBarDrawerToggle::class.java
                        .getDeclaredField("mActivityImpl")
                mActivityImplField.isAccessible = true
                val mActivityImpl = mActivityImplField.get(this)
                val getActionBarThemedContextMethod = mActivityImpl.javaClass
                        .getDeclaredMethod("getActionBarThemedContext")
                return getActionBarThemedContextMethod.invoke(mActivityImpl) as Context
            } catch (e: Exception) {
                return null
            }

        }

    constructor(activity: Activity, drawerLayout: DrawerLayout,
                openDrawerContentDescRes: Int,
                closeDrawerContentDescRes: Int) : super(activity, drawerLayout, openDrawerContentDescRes,
            closeDrawerContentDescRes) {
        init(activity)
    }

    constructor(activity: Activity, drawerLayout: DrawerLayout,
                toolbar: Toolbar, openDrawerContentDescRes: Int,
                closeDrawerContentDescRes: Int) : super(activity, drawerLayout, toolbar, openDrawerContentDescRes,
            closeDrawerContentDescRes) {
        init(activity)
    }

    fun setOnLongClickListener(toolbar: Toolbar, action: View.OnLongClickListener) {
        getNavButtonView(toolbar)?.setOnLongClickListener(action)
    }

    private fun init(activity: Activity) {
        var c = themedContext
        if (c == null) {
            c = activity
        }
        badgeDrawable = BadgeDrawerArrowDrawable(c)
        drawerArrowDrawable = badgeDrawable!!
    }

    private fun getNavButtonView(toolbar: Toolbar): ImageButton? {
        try {
            val toolbarClass = toolbar.javaClass
            val navButtonField: Field = toolbarClass.getDeclaredField("mNavButtonView")
            navButtonField.isAccessible = true
            return navButtonField.get(toolbar) as ImageButton
        } catch (ex: NoSuchFieldException) {
        } catch (ex: IllegalAccessException) {
        }

        return null
    }


    class BadgeDrawerArrowDrawable(context: Context) : DrawerArrowDrawable(context) {
        private val backgroundPaint: Paint
        private val textPaint: Paint
        var text: String? = null
            set(text) {
                if (this.text != text) {
                    field = text
                    invalidateSelf()
                }
            }
        var isEnabled = true
            set(enabled) {
                if (this.isEnabled != enabled) {
                    field = enabled
                    invalidateSelf()
                }
            }

        var backgroundColor: Int
            get() = backgroundPaint.color
            set(color) {
                if (backgroundPaint.color != color) {
                    backgroundPaint.color = color
                    invalidateSelf()
                }
            }

        var textColor: Int
            get() = textPaint.color
            set(color) {
                if (textPaint.color != color) {
                    textPaint.color = color
                    invalidateSelf()
                }
            }

        init {
            backgroundPaint = Paint()
            backgroundPaint.color = Color.RED
            backgroundPaint.isAntiAlias = true
            textPaint = Paint()
            textPaint.color = Color.WHITE
            textPaint.isAntiAlias = true
            textPaint.typeface = Typeface.DEFAULT_BOLD
            textPaint.textAlign = Paint.Align.CENTER
            textPaint.textSize = SIZE_FACTOR * intrinsicHeight
        }

        override fun draw(canvas: Canvas) {
            super.draw(canvas)

            if (!isEnabled) {
                return
            }

            if (this.text == null || this.text!!.length == 0) {
                return
            }

            val bounds = bounds
            val x = (1 - HALF_SIZE_FACTOR) * bounds.width()
            val y = HALF_SIZE_FACTOR * bounds.height()
            backgroundPaint.color = Color.WHITE
            canvas.drawCircle(x, y, 1.1f * SIZE_FACTOR * bounds.width(), backgroundPaint)
            backgroundPaint.color = Color.RED
            canvas.drawCircle(x, y, SIZE_FACTOR * bounds.width(), backgroundPaint)

            val textBounds = Rect()
            textPaint.getTextBounds(this.text, 0, this.text!!.length, textBounds)
            canvas.drawText(this.text!!, x, y + textBounds.height() / 2, textPaint)
        }

        companion object {

            // Fraction of the drawable's intrinsic size we want the badge to be.
            private val SIZE_FACTOR = .3f
            private val HALF_SIZE_FACTOR = SIZE_FACTOR / 2
        }
    }
}
