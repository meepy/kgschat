package com.randomdrop.kgs.kgschat

import android.util.Log
import com.randomdrop.kgs.kgschat.MainActivity.Companion.TAG
import java.io.DataInputStream


enum class MsgTypesDown(val id: Int) {

    // "static" messages:
    HELLO(0),
    USER_UPDATE(1),
    LOGIN_FAILED_NO_SUCH_USER(2),
    LOGIN_FAILED_BAD_PASSWORD(3),
    LOGIN_FAILED_USER_ALREADY_EXISTS(4),
    RECONNECT(5),
    LOGIN_SUCCESS(6),
    ARCHIVE_JOIN(7),
    ARCHIVE_NONEXISTANT(8),
    ROOM_NAMES(9),
    PRIVATE_KEEP_OUT(10),
    DETAILS_JOIN(16),
    DETAILS_NONEXISTANT(11),
    AVATAR(12),
    ROOM_CREATED(22),
    CONVO_JOIN(23),
    CONVO_NO_SUCH_USER(24),
    MESSAGES(25),
    MESSAGE_CREATE_SUCCESS(26),
    MESSAGE_CREATE_NO_USER(27),
    MESSAGE_CREATE_FULL(28),
    MESSAGE_CREATE_CONNECTED(29),
    FRIEND_CHANGE_NO_USER(30),
    FRIEND_ADD_SUCCESS(31),
    FRIEND_REMOVE_SUCCESS(32),
    CHALLENGE_CREATED(33),
    CHALLENGE_NOT_CREATED(34),
    IDLE_WARNING(36),
    SYNC(38),
    GAME_NOTIFY(40),
    GAME_REVIEW(48),
    LOAD_FAILED(49),
    CHANNEL_AUDIO(-50),
    PLAYBACK_ADD(51),
    PLAYBACK_SETUP(52),
    GLOBAL_GAMES_JOIN(54),
    SUBSCRIPTION_UPDATE(55),
    ANNOUNCEMENT(57),
    SERVER_STATS(58),
    DELETE_ACCOUNT_ALREADY_GONE(59),
    DELETE_ACCOUNT_SUCCESS(60),
    CHANNEL_SUBSCRIBERS_ONLY(61),
    LOGIN_FAILED_KEEP_OUT(65),
    TOO_MANY_KEEP_OUTS(66),
    KEEP_OUT_SUCCESS(67),
    KEEP_OUT_LOGIN_NOT_FOUND(68),
    CLEAR_KEEP_OUT_SUCCESS(69),
    CLEAR_KEEP_OUT_FAILURE(70),
    CANT_PLAY_TWICE(71),
    ALREADY_IN_PLAYBACK(72),
    GAME_STARTED(73),
    REGISTER_SUCCESS(74),
    REGISTER_BAD_EMAIL(75),
    PLAYBACK_ERROR(77),
    ROOM_CREATE_TOO_MANY_ROOMS(78),
    ROOM_CREATE_NAME_TAKEN(79),
    ROOM_CHANNEL_INFO(80),
    PLAYBACK_DELETE(83),
    SUBSCRIPTION_LOW(85),
    AD(93),
    TOURN_NOTIFY(96),
    AUTOMATCH_PREFS(99),
    AUTOMATCH_STATUS(100),
    FETCH_TAGS_RESULT(102),
    TAG_ARCHIVE_JOIN(103),
    TAG_ARCHIVE_NONEXISTANT(104),
    LOGOUT(106),

    // messages with a channel target:
    UNJOIN(-1),
    ARCHIVE_GAMES_CHANGED(-2),
    ARCHIVE_GAME_REMOVED(-3),
    CLOSE(-4),
    JOIN(-11),
    USER_ADDED(-12),
    CHAT(-13),
    ANNOUNCE(-14),
    USER_REMOVED(-15),
    DETAILS_UPDATE(-17),
    DETAILS_RANK_GRAPH(-18),
    ROOM_CAT_COUNTERS(-19),
    ROOM_DESC(-20),
    ROOM_NAME_FLUSH(-21),
    CHALLENGE_CANT_PLAY_RANKED(-35),
    GAME_LIST(-37),
    CHALLENGE_PROPOSAL(-39),
    GAME_STATE(-41),
    GAME_UPDATE(-42),
    GAME_UNDO_REQUEST(-43),
    MODERATED_CHAT(-44),
    SET_CHAT_MODE(-45),
    GAME_TIME_EXPIRED(-46),
    GAME_OVER(-47),
    PLAYBACK_DATA(-53),
    CHALLENGE_FINAL(-56),
    GAME_ALL_PLAYERS_GONE(-62),
    GAME_EDITOR_LEFT(-63),
    CHANNEL_NO_TALKING(-64),
    GAME_CONTAINER_REMOVE_GAME(-76),
    CHANNEL_CHANGE_NO_SUCH_USER(-81),
    ACCESS_LIST(-82),
    CHALLENGE_SUBMITTED(-84),
    ROOM_CAT_ROOM_GONE(-86),
    CONVO_NO_CHATS(-87),
    GAME_SET_ROLES_UNKNOWN_USER(-88),
    GAME_SET_ROLES_NOT_MEMBER(-89),
    CHALLENGE_DECLINED(-90),
    GAMELISTENTRY_PLAYER_REPLACED(-91),
    CHANNEL_AD(-92),
    CHANNEL_CHAT_ALLOWED(-94),
    CHANNEL_ALREADY_JOINED(-95),
    GAME_PREP_STATUS(-96),
    PLAYBACK_SEEK_START(-97),
    PLAYBACK_SEEK_COMPLETE(-98),
    GAME_NAME_CHANGE(-101),
    JOIN_COMPLETE(-105),
    ROOM_JOIN(-106),
    GAME_JOIN(-107),
    CHALLENGE_JOIN(-108),
    GAME_LOOP_WARNING(-254),

    UNKNOWN(666);

    companion object {
        private val msgMap = HashMap<Int, MsgTypesDown>()

        init {
            for (type in values()) {
                msgMap[type.id] = type
            }
        }

        fun idToMsgType(id: Int): MsgTypesDown? {
            return msgMap[id]
        }
    }

    // Event handling system

    private var handlers = listOf<(MsgTypesDown, Int, DataInputStream) -> Unit>()

    @Synchronized
    infix fun on(handler: (MsgTypesDown, Int, DataInputStream) -> Unit) {
        if (handlers.isNotEmpty()) // TODO: possible problem here
            Log.w(TAG, "multiple MsgDown handlers for $name")
        handlers += handler
    }

    @Synchronized
    infix fun off(handler: (MsgTypesDown, Int, DataInputStream) -> Unit) {
        handlers -= handler
    }

    @Synchronized
    fun emit(inputStream: DataInputStream) {
        if (handlers.isNotEmpty()) {
            Log.d(TAG, "$name: handling...")
        } else {
            Log.d(TAG, "$name: unhandled")
        }

        val channelID = if (id < 0) inputStream.readInt() else 0
        for (subscriber in handlers) {
            subscriber(this, channelID, inputStream)
        }
    }
}
