package com.randomdrop.kgs.kgschat

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.support.constraint.ConstraintLayout
import android.text.Html
import android.text.Html.FROM_HTML_MODE_COMPACT
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.BackgroundColorSpan
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.pixplicity.htmlcompat.HtmlCompat
import com.randomdrop.kgs.kgschat.model.*
import kotlinx.android.synthetic.main.chatbox.view.*
import java.lang.Thread.sleep
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import kotlin.concurrent.thread


open class ChatBoxView(val channel: Channel, context: Context) : ConstraintLayout(context) {
    var isAtBottom: Boolean = true
    var isJoin: Boolean = false

    var lastReadID = 0
    var unreadMessages = 0

    private val textView: TextView
    private val scrollView: ScrollView
    private val chatLayout: LinearLayout
    private var mViewTreeObserver: ViewTreeObserver? = null

    protected val chatText: EditText
    protected val chatButton: Button

    init {
        LayoutInflater.from(context).inflate(R.layout.chatbox, this, true)
        textView = findViewById(R.id.textViewChat)
        scrollView = findViewById(R.id.scrollViewChat)
        chatLayout = findViewById(R.id.chatLayout)
        chatText = findViewById(R.id.editTextChat)
        chatButton = findViewById(R.id.buttonChat)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            scrollView.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
//                Log.d(MainActivity.TAG, "scroll change listener 1")
                didWeHitBottom()
            }
        } // old versions handled in onLayoutChange

        scrollToBottom.setOnClickListener {
//            Log.d(MainActivity.TAG, "scroll to bottom click")
            fullScroll()
        }

        chatLayout.addOnLayoutChangeListener(this::onLayoutChange)
        chatButton.setOnClickListener(this::chatButtonClicked)
        chatText.setOnEditorActionListener(this::enterKeyPressed)
    }

    private fun onLayoutChange(v: View, left: Int, top: Int, right: Int, bottom: Int,
                               oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int) {
//        Log.d(MainActivity.TAG, "layout change")
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (mViewTreeObserver != chatLayout.viewTreeObserver) {
                mViewTreeObserver = chatLayout.viewTreeObserver
                mViewTreeObserver?.addOnScrollChangedListener {
//                    Log.d(MainActivity.TAG, "scroll change listener 2")
                    if (this@ChatBoxView.visibility == View.VISIBLE) {
                        didWeHitBottom()
                    }
                }
            }
        }
        if (isAtBottom) {
            fullScroll()
        }
    }

    private fun didWeHitBottom() {
        val view = scrollView.getChildAt(0)
        val scrollY = scrollView.scrollY
        val h1 = scrollView.height
        val h2 = view.height

//        Log.d(MainActivity.TAG, "dirty: ${view.isDirty}, laidout: ${view.isLaidOut}")
//        Log.d(MainActivity.TAG, "didWeHitBottom: ${h1 + scrollY >= h2}, scrollY: $scrollY + height: $h1 >= $h2")
        isAtBottom = h1 + scrollY >= h2
        if (isAtBottom) {
            scrollToBottom.visibility = GONE
            clearUnreadMessages()
        } else {
            scrollToBottom.visibility = VISIBLE
        }
    }

    private fun fullScroll() {
        scrollView.post {
//            Log.d(MainActivity.TAG, "fullScroll")
            val view = scrollView.getChildAt(0)
            clearUnreadMessages()

            val deltaY = view.height - scrollView.height
            if (deltaY > 0) {
//                Log.d(MainActivity.TAG, "fullScroll: $deltaY")
                scrollView.scrollY = deltaY
//                Log.d(MainActivity.TAG, "fullScroll done")
            }
        }
    }

    private fun enterKeyPressed(view: TextView, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_SEND ||
            event?.keyCode == KeyEvent.KEYCODE_ENTER && event?.action == KeyEvent.ACTION_DOWN) {

            sendChat()
            return true
        }
        return false
    }

    private fun chatButtonClicked(view: View) {
        if (isJoin)
            Commands.convoRequest((channel as PrivateConvo).peer.name)
        else
            sendChat()
    }

    private fun sendChat() {
        if (!channel.connected)
            return
        Commands.sendChat(channel.channelID, chatText.text.toString(), false, false)
        chatText.text.clear()
        chatText.clearFocus()
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(chatText.windowToken, 0)
    }

    private var unreadHandler: ((lastReadID: Int, numUnread: Int) -> Unit)? = null

    fun onUnreadMessages(action: (lastReadID: Int, numUnread: Int) -> Unit) {
        unreadHandler = action
    }

    fun clearUnreadMessages() {
        lastReadID += unreadMessages
        unreadMessages = 0
        unreadHandler?.invoke(lastReadID, unreadMessages)
    }

    fun clearAllMessages() {
        val lastKeepFading = keepFading.getAndSet(false)
        while (stillFading.get() > 0)
            sleep(20)
        channel.clear()
        textView.post {
            textView.text = ""
        }
        lastReadID += unreadMessages
        unreadMessages = 0
        unreadHandler?.invoke(lastReadID, unreadMessages)
        keepFading.set(lastKeepFading)
    }

    private fun doUnreadMessages(lastMsgID: Int) {
        if (isAtBottom && visibility == View.VISIBLE) {
            lastReadID = lastMsgID
        } else {
            unreadMessages = lastMsgID - lastReadID
            unreadHandler?.invoke(lastReadID, unreadMessages)
        }
    }

    private fun processMsg(ev: ChatEvent) {
        if (ev.channelID == channel.channelID) {
            textView.post {
                val sb = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Html.fromHtml(ev.msg, FROM_HTML_MODE_COMPACT) as SpannableStringBuilder
                } else {
                    HtmlCompat.fromHtml(this.context, ev.msg, HtmlCompat.FROM_HTML_MODE_COMPACT) as SpannableStringBuilder
                }
                val start = textView.length()
                textView.append(sb)
                if (MainActivity.prefs.highlightNewMessages)
                    fadeBackground(start, sb.length, sb)

                doUnreadMessages(ev.lastMsgID)
            }
        }
    }

    private val keepFading = AtomicBoolean(true)
    private val stillFading = AtomicInteger(0)

    private fun fadeBackground(start: Int, length: Int, sp: SpannableStringBuilder) {
        thread {
            stillFading.incrementAndGet()
            var bgdSpan: BackgroundColorSpan? = null
            var alpha = .5f

            while (alpha >= 0 && keepFading.get()) {
                textView.post {
                    if (bgdSpan != null)
                        sp.removeSpan(bgdSpan)
                    val color = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        Color.valueOf(0f, .5f, 0f, alpha).toArgb()
                    } else {
                        ((alpha * 255f).toInt() shl 24) or 0x00007F00
                    }
                    bgdSpan = BackgroundColorSpan(color)
                    sp.setSpan(bgdSpan, 0, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                    textView.editableText.replace(start, start + length, sp)
                }
                sleep(250)
                alpha -= .5f / 40f
            }
            textView.post {
                if (bgdSpan != null)
                    sp.removeSpan(bgdSpan)
                bgdSpan = BackgroundColorSpan(Color.TRANSPARENT)
                sp.setSpan(bgdSpan, 0, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                textView.editableText.replace(start, start + length, sp)
            }
            stillFading.decrementAndGet()
        }
    }

    open fun startListening() {
        val sp = SpannableStringBuilder()
        val lastMsgID = channel.getMessages {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                sp.append(Html.fromHtml(it, FROM_HTML_MODE_COMPACT))
            } else {
                sp.append(HtmlCompat.fromHtml(this.context, it, HtmlCompat.FROM_HTML_MODE_COMPACT))
            }
        }

        textView.text = sp

        doUnreadMessages(lastMsgID)
        ChatEvent.on(this::processMsg)
        thread {
            sleep(250)
            keepFading.set(true)
        }

        processStatus(ChannelStatusEvent(channel.channelID, channel.connected))
        ChannelStatusEvent.on(this::processStatus)
    }

    open fun stopListening() {
        keepFading.set(false)
        ChatEvent.off(this::processMsg)
        ChannelStatusEvent.off(this::processStatus)
    }

    private fun processStatus(ev: ChannelStatusEvent) {
        if (ev.channelID == channel.channelID) {
            post {
                setConvoStatus()
            }
        }
    }

    protected open fun setConvoStatus() {
        if (channel.connected) {
            chatText.hint = "Chat Here"
            chatText.isEnabled = true
            chatButton.isEnabled = true
        } else {
            chatText.text.clear()
            chatText.hint = "Chat Disabled: No Connection"
            chatText.isEnabled = false
            chatButton.isEnabled = false
        }
    }

    open fun onPersonButtonClick(view: View, model: Model) = PopupMenu(view.context, view).run {
        menuInflater.inflate(R.menu.users_popup_menu, menu)
        val names = (channel as Room).users.keys
        for (name in names.sorted()) {
            menu.add(name)
        }

        if (menu.size() == 0) {
            Toast.makeText(view.context, "No users to show", Toast.LENGTH_SHORT).show()
        } else {
            setOnMenuItemClickListener { item ->
                val name = item.title.toString()
                if (name == model.me?.name) {
                    Toast.makeText(view.context, "So you'd like to talk to yourself?", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(view.context, "Opening conversation with: $name", Toast.LENGTH_SHORT).show()
                    Commands.convoRequest(name)
                }
                true
            }
            show()
        }
    }
}