package com.randomdrop.kgs.kgschat

import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.DialogFragment
import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.animation.LinearInterpolator
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import com.randomdrop.kgs.kgschat.model.ErrorEvent
import com.randomdrop.kgs.kgschat.model.FriendGroupTypes
import com.randomdrop.kgs.kgschat.model.Model
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity(), FriendGroupDialog.FriendGroupDialogListener,
        ClearChatDialog.ClearChatListener {

    companion object {
        @SuppressLint("StaticFieldLeak")
        var context: Context? = null

        const val TAG = "KGSCHAT"

        var prefs = Prefs()
    }

    private val model = Model()
    private val navigator = Navigation(this)
    private val loginView = LoginView(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        context = applicationContext

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        doLadderBackground()
//        doRainBowBackground()

        loginView.start(model)
        navigator.start(model)
        model.startListening(this)

        val personButton = findViewById<Button>(R.id.button1)
        when (Random.nextInt(3)) {
            0 -> personButton.setBackgroundResource(R.drawable.ic_person_woman)
            1 -> personButton.setBackgroundResource(R.drawable.ic_person_man)
            2 -> personButton.setBackgroundResource(R.drawable.ic_person_ninja)
        }

        prefs.readDefaults(getPreferences(Context.MODE_PRIVATE))
    }

    private fun doLadderBackground() {

        val imgDimInDP = 208f
        val stepDimInDP = (42f / 512f) * imgDimInDP
        val imgDim = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                imgDimInDP, resources.displayMetrics)
        val stepDim = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                stepDimInDP, resources.displayMetrics)

        val height = Resources.getSystem().displayMetrics.heightPixels
        val width = Resources.getSystem().displayMetrics.widthPixels
        val animator = ValueAnimator.ofInt(0, 4 * ((width + 2f * imgDim) / stepDim).toInt())
        animator.repeatCount = ValueAnimator.INFINITE
        animator.interpolator = LinearInterpolator()
        animator.duration = 50000L

        var deltaX = -imgDim
        val randomPos = Random.nextInt(width / 4, height + width / 4)
        if (randomPos > height)
            deltaX += (randomPos - height).toFloat()
        var deltaY = if (randomPos <= height) -randomPos.toFloat() else
            -height.toFloat()

        background1.translationX = deltaX
        background1.translationY = deltaY
        background2.translationX = deltaX
        background2.translationY = deltaY
        background3.translationX = deltaX
        background3.translationY = deltaY
        background4.translationX = deltaX
        background4.translationY = deltaY

        animator.addUpdateListener {
            val progress = it.animatedValue as Int
            if (progress == 0) {
                val height = Resources.getSystem().displayMetrics.heightPixels
                val width = Resources.getSystem().displayMetrics.widthPixels
                deltaX = -imgDim
                val randomPos = Random.nextInt(width / 4, height + width / 4)
                if (randomPos > height) {
                    deltaX += (randomPos - height).toFloat()
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                        animator.setCurrentFraction((randomPos - height).toFloat() /
                                (width + 2f * imgDim))
                    }
                }
                deltaY = if (randomPos <= height) -randomPos.toFloat() else
                    -height.toFloat()

            }
            if (progress % 4 == 0) {
                val dX = deltaX + progress * stepDim / 4
                val dY = deltaY + progress * stepDim / 4
                background1.translationX = dX
                background1.translationY = dY
                background2.translationX = dX
                background2.translationY = dY
                background3.translationX = dX
                background3.translationY = dY
                background4.translationX = dX
                background4.translationY = dY
                if (deltaY > imgDim && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                    animator.setCurrentFraction(1f)
                }
            }
            background1.visibility = if (progress % 4 == 0) VISIBLE else GONE
            background2.visibility = if (progress % 4 == 1) VISIBLE else GONE
            background3.visibility = if (progress % 4 == 2) VISIBLE else GONE
            background4.visibility = if (progress % 4 == 3) VISIBLE else GONE
        }
        animator.start()
    }

    private fun doRainBowBackground() {
        val background1 = ImageView(context)
        drawer_layout.addView(background1, 0)
        background1.layoutParams.height = DrawerLayout.LayoutParams.MATCH_PARENT
        background1.layoutParams.width = DrawerLayout.LayoutParams.MATCH_PARENT

        val rainbow = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,
                arrayOf(Color.RED, Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.RED).toIntArray())
        background1.setImageDrawable(rainbow)
        background1.alpha = .5f

        val background2 = ImageView(context)
        drawer_layout.addView(background2, 0)
        background2.layoutParams.height = DrawerLayout.LayoutParams.MATCH_PARENT
        background2.layoutParams.width = DrawerLayout.LayoutParams.MATCH_PARENT

        val rainbow2 = GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT,
                arrayOf(Color.RED, Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.RED).toIntArray())
        background2.setImageDrawable(rainbow2)
        background2.alpha = .5f

        val animator = ValueAnimator.ofFloat(0f, 1f)
        animator.repeatCount = ValueAnimator.INFINITE
        animator.interpolator = LinearInterpolator()
        animator.duration = 10000L
        animator.addUpdateListener {
            val progress = it.animatedValue as Float
            background1.translationX = content_view.width * progress
            background2.translationX = content_view.width * progress - content_view.width
        }
        animator.start()
    }

    override fun onPause() {
        super.onPause()

        loginView.stopListening()
        navigator.stopListening()
        ErrorEvent.off(this::processError)
    }

    override fun onResume() {
        super.onResume()

        loginView.startListening()
        navigator.startListening()
        ErrorEvent.on(this::processError)
    }

    override fun onDestroy() {
        super.onDestroy()
        loginView.stop()
        model.stopListening()
    }

    override fun onBackPressed() {
        navigator.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu. this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_settings -> {
            val dialog = SettingsDialog()
            dialog.show(fragmentManager, "SettingsDialog")
            true
        }
        R.id.action_buddies -> {
            val titleString = resources.getString(R.string.action_buddies)
            val dialog = FriendGroupDialog().apply {
                title = titleString
                groupType = FriendGroupTypes.BUDDY
                itemArray = model.users.buddied.toNameList().sorted()
            }
            dialog.show(fragmentManager, "BuddyGroupDialog")
            true
        }
        R.id.action_censored -> {
            val titleString = resources.getString(R.string.action_censored)
            val dialog = FriendGroupDialog().apply {
                title = titleString
                groupType = FriendGroupTypes.CENSOR
                itemArray = model.users.censored.toNameList().sorted()
            }
            dialog.show(fragmentManager, "CensorGroupDialog")
            true
        }
        R.id.action_fanned -> {
            val titleString = resources.getString(R.string.action_fanned)
            val dialog = FriendGroupDialog().apply {
                title = titleString
                groupType = FriendGroupTypes.FAN
                itemArray = model.users.fanned.toNameList().sorted()
            }
            dialog.show(fragmentManager, "FanGroupDialog")
            true
        }
        android.R.id.home -> {
            navigator.openDrawer()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun processError(ev: ErrorEvent) {
        runOnUiThread {
            Toast.makeText(context, ev.msg, Toast.LENGTH_SHORT).show()
        }
    }

    fun onPersonButtonClick(view: View) {
        if (navigator.currentView is ChatBoxView)
            (navigator.currentView as ChatBoxView).onPersonButtonClick(view, model)
    }

    fun loginRegular(view: View) {
        loginView.login()
    }

    fun loginGuest(view: View) {
        loginView.guest()
    }

    fun logout(view: View) {
        loginView.logout()
    }

    fun showClearChatDialog() {
        val dialog = ClearChatDialog()
        dialog.show(fragmentManager, "ClearChatDialog")
    }

    fun closeDetails(view: View) {
        val cV = navigator.currentView
        if (cV != null && cV is PrivateConvoView) {
            cV.closeDetails()
        }
    }

    override fun onDialogItemClick(dialog: DialogFragment) = when (dialog) {
        is FriendGroupDialog -> {
            val name = dialog.selected
            if (name == model.me?.name) {
                Toast.makeText(context, "So you'd like to talk to yourself?", Toast.LENGTH_SHORT).show()
            } else if (name != null &&  name.isNotEmpty()) {
                Toast.makeText(context, "Opening conversation with: $name", Toast.LENGTH_SHORT).show()
                Commands.convoRequest(name)
            }
            Unit
        }
        else -> Unit
    }

    override fun onDialogOKClick(dialog: DialogFragment) = when (dialog) {
        is ClearChatDialog -> {
            if (dialog.selectedClearNew) {
                for ((_, chatView) in navigator.allChatRooms) {
                    chatView.clearUnreadMessages()
                }
            }
            if (dialog.selectedClearAll) {
                for ((_, chatView) in navigator.allChatRooms) {
                    chatView.clearAllMessages()
                }
            }
            Unit
        }
        else -> Unit
    }
}
