package com.randomdrop.kgs.kgschat

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle

class ExitDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            builder.setMessage("Exit application?")
                .setPositiveButton(R.string.ok) { dialog, id ->
                    activity?.finish()
                }
                .setNegativeButton(R.string.cancel) { dialog, id ->
                }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}