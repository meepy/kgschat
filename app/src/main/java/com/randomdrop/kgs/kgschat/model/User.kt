package com.randomdrop.kgs.kgschat.model

data class User(val name: String, var flags: Int) {

    val notes = arrayOf("", "", "", "")

    // Convenience properties for dealing with flags:

    val isDeleted: Boolean
        get() = (flags and 0x20) != 0

    val isConnected: Boolean
        get() = (flags and 0x10) != 0

    val isInTournament: Boolean
        get() = (flags and 0x10000) != 0

    val isPlaying: Boolean
        get() =  (flags and 0x8000) != 0

    val isSleeping: Boolean
        get() = (flags and 0x40) != 0

    val isHelpful: Boolean
        get() = (flags and 0x1000) != 0

    val isGuest: Boolean
        get() = (flags and 0x8) != 0

    val isRobot: Boolean
        get() = (flags and 0x80) != 0

    val isRankWanted: Boolean
        get() = (flags and 0x100) != 0
    val isRankConfident: Boolean
        get() = (flags and 0x200) != 0

    val isAvatar: Boolean
        get() = (flags and 0x400) != 0

    val isTournWinner: Boolean
        get() = (flags and 0x2000) != 0

    val isTournRunnerUp: Boolean
        get() = (flags and 0x4000) != 0

    val isSubscribed: Boolean
        get() = (flags and 0x800) != 0

    val isLowBandwidth: Boolean
        get() = (flags and 0x20000) != 0

    val isLongLived: Boolean
        get() = (flags and 0x40000) != 0

    val isMeijin: Boolean
        get() = (flags and 0x80000) != 0

    val canPlayRanked: Boolean
        get() = (flags and 0x108) == 0x100

    val rank: Int
        get() {
            return if ((flags and 0x100) == 0)
                0
            else
                (flags shr 20) and 0x3ff
        }

    val authLevel: AuthLevel
        get() = AuthLevel.values()[flags and 0x7]

    enum class AuthLevel {
        NORMAL, ROBOT_RANKED, TEACHER, JR_ADMIN, SR_ADMIN, SUPER_ADMIN;
    }
}