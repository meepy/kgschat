package com.randomdrop.kgs.kgschat.model

import android.text.Html.escapeHtml
import com.randomdrop.kgs.kgschat.Event
import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random

abstract class Channel(val channelID: Int, var name: String) {
    var connected: Boolean = true // start up connected
    val messages = LinkedList<String>()
    var lastMsgID = 0

    companion object {
        private const val MAX_SIZE = 10000
        const val KGSX_MARKER = "🎲"
        const val LIGHTGRAY = "#a8a8a8"

        fun fixStupidUsers(user: String, msg: String): String {
            var fixed = msg

            when (user.toUpperCase()) {
                "OWYHEEMUD" ->
                    fixed = fixed.replace("bye",
                            "<span style='color:$LIGHTGRAY'>by</span>")

                "TOMICUS", "TOMI" -> {
                    val separated = fixed.split(' ').toMutableList()
                    if (Random.nextFloat() < 0.4f) {
                        val position = Random.nextInt(separated.size)
                        val burp = "<span style='color:$LIGHTGRAY'>" +
                                (if (Random.nextBoolean()) "*urp*" else "*hic*") +
                                "</span>"
                        separated.add(position, burp)
                    }
                    fixed = separated.joinToString(" ")
                }
            }

            return fixed
        }
    }

    @Synchronized
    open fun addMessage(user: User?, content: String, isAnnounce: Boolean = false,
                        isMe: Boolean = false, isCounted: Boolean = true): String? {

        if (messages.size == MAX_SIZE)
            messages.remove()

        if (content.startsWith(KGSX_MARKER)) // TODO: process kgsX messages
            return null

        val fixedUserName = if (user != null) user.name + ": " else ""
        val fixedContent = fixStupidUsers(user?.name ?: "", escapeHtml(content))
                .replace("\n", "</p><p>")

        val now = Calendar.getInstance()
        val timeStamp = SimpleDateFormat("HH:mm:ss").format(now.time)
        var styleStart = ""
        var styleEnd = ""

        if (isAnnounce) {
            styleStart = "$styleStart<b>"
            styleEnd = "</b>$styleEnd"
        }

        if (isMe) {
            styleStart = "$styleStart<span style='color:#4d8c51'>"
            styleEnd = "</span>$styleEnd"
        }

        val msg = "<p><small><span style='color:$LIGHTGRAY'>[$timeStamp]</span></small> $styleStart$fixedUserName$fixedContent$styleEnd</p>"
        messages.add(msg)

        if (isCounted)
            lastMsgID++

        return msg
    }

    // processes all messages and returns the last ID
    @Synchronized
    open fun getMessages(processMsg: (String) -> Unit): Int {
        for (msg in messages)
            processMsg(msg)
        return lastMsgID
    }

    @Synchronized
    open fun connectionStatus(connected: Boolean) {
        if (connected && !this.connected) {
            this.connected = true
            addMessage(null, "Opened Connection", isAnnounce = true, isCounted = false)
            ChannelStatusEvent(channelID, connected).emit()
        } else if (!connected && this.connected) {
            this.connected = false
            addMessage(null, "Closed Connection", isAnnounce = true, isCounted = false)
            ChannelStatusEvent(channelID, connected).emit()
        }
    }

    @Synchronized
    fun copy(other: Channel) {
        messages.clear()
        messages.addAll(other.messages)
    }

    @Synchronized
    fun clear() {
        messages.clear()
    }
}

open class ChatEvent(val channelID: Int, val msg: String, val lastMsgID: Int) {
    companion object : Event<ChatEvent>()

    fun emit() = emit(this)
}

class PrivateChatEvent(channelID: Int, msg: String, lastMsgID: Int) :
        ChatEvent(channelID, msg, lastMsgID)

class RoomChatEvent(channelID: Int, msg: String, lastMsgID: Int) :
        ChatEvent(channelID, msg, lastMsgID)

open class ChannelStatusEvent(val channelID: Int, val connected: Boolean) {
    companion object : Event<ChannelStatusEvent>()

    fun emit() = emit(this)
}

