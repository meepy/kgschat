package com.randomdrop.kgs.kgschat.model

import com.randomdrop.kgs.kgschat.Event

class PrivateConvo(channelID: Int, val peer: User) :
            Channel(channelID, "PM: ${peer.name}"){

    private var state = State.NORMAL
    var newConvo: PrivateConvo? = null // NOTE: convenience for peer reconnects

    var chatsBlocked: Boolean // NOTE: admin level may ignore this
        @Synchronized
        get() = !state.isChatOK
        @Synchronized
        set(value) {
            when (value) {
                true -> if (state != State.CHATS_BLOCKED) {
                    state = State.CHATS_BLOCKED
                    ConvoStateChanged(channelID).emit()
                }

                false -> if (state == State.OLD_CHANNEL || state == State.CHATS_BLOCKED) {
                    state = State.INVALID
                    update()
                }
            }
        }

    val stateMsg: String
        @Synchronized
        get() = state.msg

    val isOldChannel: Boolean
        @Synchronized
        get() = state == State.OLD_CHANNEL

    @Synchronized
    fun reset() {
        connectionStatus(true)
        chatsBlocked = false
        newConvo = null
    }

    @Synchronized
    fun setStale() {
        if (state != State.OLD_CHANNEL) {
            state = State.OLD_CHANNEL
            ConvoStateChanged(channelID).emit()
        }
    }

    @Synchronized
    override fun connectionStatus(connected: Boolean) {
        if (!this.connected && connected) {
            state = State.OLD_CHANNEL
        }
        super.connectionStatus(connected)
    }

    @Synchronized
    override fun addMessage(user: User?, content: String, isAnnounce: Boolean,
                   isMe: Boolean, isCounted: Boolean): String? {

        val msg = super.addMessage(user, content, isAnnounce, isMe, isCounted)

        if (msg != null && user != null)
            chatsBlocked = false

        if (msg != null)
            PrivateChatEvent(channelID, msg, lastMsgID).emit()

        return msg
    }

    @Synchronized
    fun update() {
        val newState = if (peer.isDeleted) {
            State.PEER_GONE
        } else if (!peer.isConnected) {
            State.PEER_DISCONNECTED
        } else if (peer.isInTournament) {
            State.PEER_IN_TOURNAMENT
        } else if (state == State.CHATS_BLOCKED) { // NOTE: playing and sleeping ignored
            State.CHATS_BLOCKED
        } else if (peer.isPlaying) {
            State.PEER_PLAYING
        } else if (peer.isSleeping) {
            State.PEER_SLEEPING
        } else {
            State.NORMAL
        }

        if (newState != state) {
            when (state) {
                State.PEER_GONE -> {
                    state = State.OLD_CHANNEL
                    ConvoStateChanged(channelID).emit()
                }
                State.OLD_CHANNEL -> Unit
                else -> {
                    state = newState
                    ConvoStateChanged(channelID).emit()
                }
            }
        }
    }

    enum class State(val isChatOK: Boolean, val msg: String, val longMsg: String) {
        PEER_GONE(false, "(Account deleted)", "This is the title of a panel with a private conversation after your friend has not only logged out, but their account has been destroyed."),
        PEER_DISCONNECTED(false, "(Disconnected)", "This is the title of a panel with a private conversation after your friend has logged out."),
        PEER_PLAYING(true, "(Playing a game)", "This is the title of a panel with a private conversation when your friend is playing a game."),
        PEER_SLEEPING(true, "(Sleeping)", "This is the title of a panel with a private conversation when your friend has been idle for a while."),
        NORMAL(true, "", "This is the title of a panel with a private conversation."),
        PEER_IN_TOURNAMENT(false, "(In tournament; no talking)", "This is the title of a panel with a private conversation."),
        CHATS_BLOCKED(false, "(Blocked; no talking)", "This is the title of a panel with a private conversation."),
        OLD_CHANNEL(false, "(Invalid Channel)", "Both are connected but this is the old channel"),
        INVALID(false, "(Invalid state)", "The state is invalid due to an update in progress"),
    }
}

data class ConvoStateChanged(val channelID: Int) {
    companion object : Event<ConvoStateChanged>()

    fun emit() = emit(this)
}
