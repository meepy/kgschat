package com.randomdrop.kgs.kgschat

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.util.Log
import kotlin.concurrent.thread

class KGSSocketService : Service() {

    companion object {
        const val ACTION_LOGIN = "log in to kgs"
        const val ACTION_SEND = "send message to kgs"
        const val ACTION_CLOSE = "close connection"

        const val TAG2 = "KGSSOCKET"

        private const val NOTIFICATION_ID = 111
        private const val NOTIFICATION_CHANNEL = "default"
    }

    private var conn: Connection? = null

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG2, "KGSSocketService created")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        when (intent?.action) {
            ACTION_LOGIN -> {
                if (conn == null) {
                    Log.d(TAG2, "KGSSocketService started")
                } else {
                    Log.d(TAG2, "KGSSocketService restarted")
                }
                startForegroundService()
                conn = Connection().also { it.start() }
            }
            ACTION_SEND -> {
                if (conn != null && !(conn!!.closing)) {
                    val tx = intent.extras.getByteArray("tx")
                    thread (start = true) {  // TODO: look into IntentService and HandlerThread
                        conn!!.send(tx)
                    }
                }
            }
            ACTION_CLOSE -> {
                stopForeground(true)
                Log.d(TAG2, "KGSSocketService stopping")
                conn?.stopConnection()
            }
        }

        return START_STICKY
    }

    override fun onDestroy() {
        stopForeground(true)
        Log.d(TAG2, "KGSSocketService destroyed")
        conn?.stopConnection()
    }

    private fun startForegroundService() {
        if (Build.VERSION.SDK_INT >= 26) {
            val nM = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channel = NotificationChannel(NOTIFICATION_CHANNEL, "KGSSocketService",
                    NotificationManager.IMPORTANCE_DEFAULT)
            channel.description = "KGS Background Service channel"
            nM.createNotificationChannel(channel)
        }

        val intent = applicationContext.packageManager.
                getLaunchIntentForPackage(applicationContext.packageName).apply {
            setPackage(null)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, 0)

        val notification = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
                .setContentTitle("KGS Chat Service")
                .setContentText("connected to KGS...")
                .setSmallIcon(R.drawable.ic_notification)
//                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.id.icon))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .build()

        startForeground(NOTIFICATION_ID, notification)
    }
}
