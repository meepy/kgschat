package com.randomdrop.kgs.kgschat

import java.io.ByteArrayOutputStream
import java.io.DataOutputStream
import java.io.IOException
import java.math.BigInteger

object RsaCrypto {
    private const val CRYPTO_PUBLIC = "fffd"
    private const val CRYPTO_MODULUS = "95226c6c0cf547afda395d5a8943c191f34131ca8727b1f3122bd" +
            "3299d7885831d79fa9b619b44f876eeba7722a7313c8bf2d1af75ff503997d6361e73b5dcb" +
            "109a897cb4bd92037877aaa72cd087ecfddae33388ed8c10c8600799285ff2082a9ea0316f" +
            "b8072d560e5966c5f7cb1295d9383c2bfd674e24e61a6af0236e49147a65713a4ad7867901" +
            "a798284f6693bfd3a0d5f11a66ff4d1cc30055763398cd56a63ca98bb28a0ccf900299b9a3" +
            "184f45f58a37b331e176c21aadead56b0696a6fa7827d138960fc9c685bd6b5a82ac407af2" +
            "64b7b093787cad484052478e8fea789de088450a44428b8c0da92ee307d35ed389200f58c4" +
            "73b7243ac4fa575"

    fun passwordCompute(password: String): Long {
        var result = 0L
        for (letter in password) {
            result = result * 1055L + letter.toLong()
        }
        return result
    }

    fun rsaEncrypt(value: Long, size: Int): ByteArray {
        val byteOut = ByteArrayOutputStream()
        val dataOut = DataOutputStream(byteOut)
        try {
            dataOut.writeLong(value)
            return encrypt(byteOut.toByteArray(), size)
        } catch (e: IOException) {
            throw RuntimeException(e)
        }
    }

    private fun encrypt(data: ByteArray, destLen: Int): ByteArray {
        val expo = BigInteger(CRYPTO_PUBLIC, 16)
        val modulus = BigInteger(CRYPTO_MODULUS, 16)

        val result = BigInteger(1, data).modPow(expo, modulus)
        val out = result.toByteArray()

        if (data.size == destLen) {
            return out
        }

        val padded = ByteArray(destLen)
        if (out.size > destLen) {
            for (i in 0 until out.size - destLen) {
                if (out[i].toInt() != 0) {
                    throw IllegalArgumentException(
                        "Wanted " + destLen + " bytes, got " + (out.size - i)
                            + ": expo=" + expo
                            + ", modulus=" + modulus + "=" + modulus.toString(16)
                            + ", data=" + hex(data) + "=" + BigInteger(1, data)
                            + ", result=" + hex(out) + "=" + result)
                }
            }
            System.arraycopy(out, out.size - destLen, padded, 0, destLen)
        } else {
            System.arraycopy(out, 0, padded, destLen - out.size, out.size)
        }
        return padded
    }

    private fun hex(data: ByteArray): String {
        val sb = StringBuilder()
        var first = true
        for (value in data) {
            if (!first) {
                sb.append(':')
            }
            first = false
            if (value <= 15) {
                sb.append('0')
            }
            sb.append(Integer.toString(value.toInt(), 16))
        }
        return sb.toString()
    }
}
