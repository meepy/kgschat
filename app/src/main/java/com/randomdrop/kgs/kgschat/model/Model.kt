package com.randomdrop.kgs.kgschat.model

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import com.randomdrop.kgs.kgschat.*
import com.randomdrop.kgs.kgschat.MainActivity.Companion.TAG
import com.randomdrop.kgs.kgschat.MainActivity.Companion.context
import java.io.ByteArrayInputStream
import java.io.DataInputStream
import java.io.PrintWriter
import java.io.StringWriter


class Model() {

    companion object {
        const val PERSONAL_NOTIFICATION_CHANNEL = "KGS CHAT MESSAGES"
    }

    lateinit var prefs: SharedPreferences

    var clientID = 9001L
    var userName = ""
    var userPassword = ""

    val rooms = RoomList()
    val convos = ConvoList() // private conversations
    val users = UserList()
    val subscriptions = Subscriptions()

    var me: User? = null

    private var notificationId = 0 // next status bar notification

    var connected = false
    var loggedIn = false
    var lastConnectionMsg = ""
    var lastLoginMsg = ""

    fun startListening(activity: MainActivity) {
        prefs = activity.getPreferences(Context.MODE_PRIVATE)
        clientID = prefs.getLong("clientID", 9001L)

        // static messages
        MsgTypesDown.HELLO.on(this::processMsgHello)
        MsgTypesDown.LOGIN_SUCCESS.on(this::processMsgLoginSuccess)
        MsgTypesDown.LOGIN_FAILED_NO_SUCH_USER.on(this::processMsgLoginFailure)
        MsgTypesDown.LOGIN_FAILED_BAD_PASSWORD.on(this::processMsgLoginFailure)
        MsgTypesDown.LOGIN_FAILED_USER_ALREADY_EXISTS.on(this::processMsgLoginFailure)
        MsgTypesDown.LOGIN_FAILED_KEEP_OUT.on(this::processMsgLoginFailure)
        MsgTypesDown.RECONNECT.on(this::processMsgLoginFailure)
        MsgTypesDown.SUBSCRIPTION_UPDATE.on(this::processMsgSubscriptionUpdate)
        MsgTypesDown.ANNOUNCEMENT.on(this::processMsgAnnouncement)
        MsgTypesDown.CONVO_NO_SUCH_USER.on(this::processMsgConvoNoSuchUser)
        MsgTypesDown.CONVO_JOIN.on(this::processMsgConvoJoin)
        MsgTypesDown.FRIEND_ADD_SUCCESS.on(this::processMsgFriendAdd)
        MsgTypesDown.FRIEND_REMOVE_SUCCESS.on(this::processMsgFriendRemove)
        MsgTypesDown.FRIEND_CHANGE_NO_USER.on(this::processMsgFriendChangeFailed)

        // targeted messages
        MsgTypesDown.CHAT.on(this::processMsgChat)
        MsgTypesDown.ANNOUNCE.on(this::processMsgChat)
        MsgTypesDown.MODERATED_CHAT.on(this::processMsgChat)
        MsgTypesDown.CLOSE.on(this::processMsgClose)
        MsgTypesDown.CONVO_NO_CHATS.on(this::processMsgConvoNoChats)

        // raw message
        ConnectionEvent.on(this::handleConnectionEvent)

        rooms.startListening()
        convos.startListening()
        users.startListening()
    }

    fun stopListening() {
        // static messages
        MsgTypesDown.HELLO.off(this::processMsgHello)
        MsgTypesDown.LOGIN_SUCCESS.off(this::processMsgLoginSuccess)
        MsgTypesDown.LOGIN_FAILED_NO_SUCH_USER.off(this::processMsgLoginFailure)
        MsgTypesDown.LOGIN_FAILED_BAD_PASSWORD.off(this::processMsgLoginFailure)
        MsgTypesDown.LOGIN_FAILED_USER_ALREADY_EXISTS.off(this::processMsgLoginFailure)
        MsgTypesDown.LOGIN_FAILED_KEEP_OUT.off(this::processMsgLoginFailure)
        MsgTypesDown.RECONNECT.off(this::processMsgLoginFailure)
        MsgTypesDown.SUBSCRIPTION_UPDATE.off(this::processMsgSubscriptionUpdate)
        MsgTypesDown.ANNOUNCEMENT.off(this::processMsgAnnouncement)
        MsgTypesDown.CONVO_NO_SUCH_USER.off(this::processMsgConvoNoSuchUser)
        MsgTypesDown.CONVO_JOIN.off(this::processMsgConvoJoin)
        MsgTypesDown.FRIEND_ADD_SUCCESS.off(this::processMsgFriendAdd)
        MsgTypesDown.FRIEND_REMOVE_SUCCESS.off(this::processMsgFriendRemove)
        MsgTypesDown.FRIEND_CHANGE_NO_USER.off(this::processMsgFriendChangeFailed)

        // targeted messages
        MsgTypesDown.CHAT.off(this::processMsgChat)
        MsgTypesDown.ANNOUNCE.off(this::processMsgChat)
        MsgTypesDown.MODERATED_CHAT.off(this::processMsgChat)
        MsgTypesDown.CLOSE.off(this::processMsgClose)
        MsgTypesDown.CONVO_NO_CHATS.off(this::processMsgConvoNoChats)

        // raw message
        ConnectionEvent.off(this::handleConnectionEvent)

        rooms.stopListening()
        convos.stopListening()
        users.stopListening()
    }

    @Synchronized
    fun setUserPassword(name: String, password: String) {
        userName = name.trim()
        userPassword = password.trim()
    }

    @Synchronized
    private fun processMsgHello(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val major = inputStream.readShort()
        val minor = inputStream.readShort()
        val serverBugfixVersion = inputStream.readShort()
//                        if (major != Config.getVERSION_MAJOR() || minor != Config.getVERSION_MINOR()) {
//                            this.emit(MAJOR_OUT_OF_DATE_EVENT);
//                            this.loginFailed(null);
//                        }
//                        else {

        val clientType = if (MainActivity.prefs.mobileClient)
                ClientType.ANDROID
            else
                ClientType.STANDALONE

        lastLoginMsg = "Logging in..."
        LoginStatus(false, lastLoginMsg).emit()
        Commands.sendLogin(inputStream.readLong(), clientType,
                clientID, userName, userPassword)
//                        }
    }

    @Synchronized
    private fun processMsgLoginSuccess(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {

        me = UserList.getUser(inputStream)

        convos.eachChannel { channel ->
            (channel as PrivateConvo).setStale()
        }

        val newId = inputStream.readLong()
        if (newId != clientID) {
            clientID = newId
            with (prefs.edit()) {
                putLong("clientID", clientID)
                apply()
            }
        }

        while (true) {
            val type = inputStream.read()
            if (type == 255) {
                break
            }

            val friend = UserList.getUser(inputStream)
            if (type >= 4) {
                continue
            }

            // read notes even for invalid user?
            val note = inputStream.readUTF()
            if (friend == null) {
                Log.d(TAG, "friend with null name")
                continue
            }

            friend.notes[type] = note

            when (type) {
                FriendGroupTypes.BUDDY.id -> users.buddied.add(friend)
                FriendGroupTypes.CENSOR.id -> users.censored.add(friend)
                FriendGroupTypes.FAN.id -> users.fanned.add(friend)
                FriendGroupTypes.ADMINWATCH.id -> users.adminwatched.add(friend)
            }
        }

        subscriptions.read(inputStream)
        rooms.readCategories(inputStream)

        while (inputStream.available() > 0) {
            if (!rooms.readRoomChanInfo(inputStream))
                break
        }

        loggedIn = true
        lastLoginMsg = "Logged in"
        LoginStatus(true, lastLoginMsg).emit()
    }

    @Synchronized
    private fun processMsgLoginFailure(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        when (msgType) {
            MsgTypesDown.LOGIN_FAILED_NO_SUCH_USER -> {
                loginFailed("The user name you gave does not exist. Please check to make sure that you typed it correctly.")
            }
            MsgTypesDown.LOGIN_FAILED_BAD_PASSWORD -> {
                loginFailed("Your password does not match the user name that you gave. Please re-type your password and try again. Make sure that caps lock is off! Your password is case-sensitive.")
            }
            MsgTypesDown.LOGIN_FAILED_USER_ALREADY_EXISTS -> {
                loginFailed("The user name you gave is already in use, so you can not use it as your guest login name. Please choose a different user name and try again.")
            }
            MsgTypesDown.LOGIN_FAILED_KEEP_OUT -> {
                loginFailed("Sorry, you cannot log in because this account or internet address is temporarily blocked. The reason for blocking is \"{0}\".")
            }
            MsgTypesDown.RECONNECT -> {
                loginFailed("You have been disconnected because another user has logged in with your account from another computer.")
            }
            else -> Unit
        }
    }

    @Synchronized
    private fun processMsgConvoNoSuchUser(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val key = inputStream.readShort()
        ErrorEvent("Unable to find user: ${Commands.origName.get(key.toInt())}").emit()
    }

    @Synchronized
    private fun processMsgConvoJoin(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val channelID = inputStream.readInt()
        val key = inputStream.readShort()
        val newIsRequested = (key.toInt() != 0)

        val peer = UserList.getUser(inputStream) ?: return

        var newConvo = convos.get(channelID)

        if (newConvo == null) {
            newConvo = PrivateConvo(channelID, peer)
        } else {
            newConvo.reset()
        }

        convos.put(channelID, newConvo)

        if (newIsRequested)
            ConvoListEvent(channelID).emit()
        else
            ConvoListEvent(null).emit()
    }

    @Synchronized
    private fun processMsgFriendAdd(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val key = inputStream.readShort() // used for failure message
        val groupType = inputStream.readByte()
        val user = UserList.getUser(inputStream) ?: return
        val note = inputStream.readUTF()

        user.notes[groupType.toInt()] = note

        when (groupType.toInt()) {
            FriendGroupTypes.BUDDY.id -> users.buddied.add(user)
            FriendGroupTypes.CENSOR.id -> users.censored.add(user)
            FriendGroupTypes.FAN.id -> users.fanned.add(user)
            FriendGroupTypes.ADMINWATCH.id -> users.adminwatched.add(user)
        }
    }

    @Synchronized
    private fun processMsgFriendRemove(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val key = inputStream.readShort() // used for failure message
        val groupType = inputStream.readByte()
        val user = UserList.getUser(inputStream) ?: return

        when (groupType.toInt()) {
            FriendGroupTypes.BUDDY.id -> users.buddied.remove(user)
            FriendGroupTypes.CENSOR.id -> users.censored.remove(user)
            FriendGroupTypes.FAN.id -> users.fanned.remove(user)
            FriendGroupTypes.ADMINWATCH.id -> users.adminwatched.remove(user)
        }
    }

    @Synchronized
    private fun processMsgFriendChangeFailed(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val key = inputStream.readShort()
        ErrorEvent("Friend group change failed for user: ${Commands.origName.get(key.toInt())}").emit()
    }

    private fun loginFailed(msg: String) {
        loggedIn = false
        lastLoginMsg = msg
        LoginStatus(false, lastLoginMsg).emit()
        Commands.close()
    }

    @Synchronized
    fun logout() {
        lastLoginMsg = "Logging out..."
        LoginStatus(loggedIn, lastLoginMsg).emit()
        Commands.close()
    }

    @Synchronized
    private fun processMsgSubscriptionUpdate(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        subscriptions.read(inputStream)
    }

    private fun processMsgAnnouncement(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        messageNotification(null, inputStream.readUTF())
    }

    private fun processMsgJoinComplete(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        // TODO: only important for Challenges
    }

    private fun processMsgAd(id: Int, inputStream: DataInputStream) {
        val widgetID = inputStream.readByte()
        val adID = inputStream.readInt()
        // TODO: the rest of buf is a raw image
    }

    private fun processMsgAdminStuff(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        var msg: String
        when (msgType) {
            MsgTypesDown.KEEP_OUT_SUCCESS -> {
                msg = "User " + inputStream.readUTF() + " has been forced off of the server."
            }
            MsgTypesDown.KEEP_OUT_LOGIN_NOT_FOUND -> {
                msg = "User " + inputStream.readUTF() + " has been forced off of the server."
            }
            MsgTypesDown.CLEAR_KEEP_OUT_SUCCESS -> {
                msg = "User " + inputStream.readUTF() + " is no longer blocked from logging in."
            }
            MsgTypesDown.CLEAR_KEEP_OUT_FAILURE -> {
                msg = "Sorry, no block matching that user exists."
            }
            MsgTypesDown.TOO_MANY_KEEP_OUTS -> {
                msg = "Sorry, you have blocked too many users recently. Please wait a few minutes then try again."
            }
        }
        // TODO: do something with this
    }

    private fun processMsgChat(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val room: Channel = rooms.rooms[channelID] ?: convos.get(channelID) ?: return

        val who = UserList.getUser(inputStream)
        val content = inputStream.readUTF()

        if (me != null && who?.name == me?.name) {
            room.addMessage(who, content, msgType == MsgTypesDown.ANNOUNCE,
                    isMe = true)
        } else {
            room.addMessage(who, content, msgType == MsgTypesDown.ANNOUNCE)
        }

        if (me != null && content.contains("@" + me!!.name, ignoreCase = true))
            messageNotification(who, content)

        Log.d(TAG, "[${room.name}] ${who?.name}: $content")
    }

    private fun processMsgClose(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val room: Channel = rooms.rooms[channelID] ?: convos.get(channelID) ?: return

        room.connectionStatus(false)
    }

    private fun processMsgConvoNoChats(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val convo = convos.get(channelID) ?: return

        convo.chatsBlocked = true
    }

    private fun messageNotification(user: User?, msg: String) {
        if (Build.VERSION.SDK_INT >= 26) {
            val nM = context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channel = NotificationChannel(PERSONAL_NOTIFICATION_CHANNEL, "KGS Chat Channel",
                    NotificationManager.IMPORTANCE_DEFAULT)
            channel.description = "Personal notifications from KGS"
            nM.createNotificationChannel(channel)
        }

        val intent = context!!.packageManager.
                getLaunchIntentForPackage(context!!.packageName).apply {
            setPackage(null)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(context!!, 0, intent, 0)

        val title = if (user == null || user.name.isEmpty())
            "Announcement"
        else
            "Personal Message"

        val message = if (user == null || user.name.isEmpty())
            msg
        else
            "${user.name}: $msg"


        val mBuilder = NotificationCompat.Builder(context!!, PERSONAL_NOTIFICATION_CHANNEL)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.ic_notification)
//                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.id.icon))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)

        if (MainActivity.prefs.soundNotification) {
            val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            mBuilder.setSound(alarmSound).setOnlyAlertOnce(true)
        }
        if (MainActivity.prefs.buzzNotification) {
            val vibratePattern = arrayOf(500L,500L,500L,500L)
            mBuilder.setVibrate(vibratePattern.toLongArray())
        }

        with (NotificationManagerCompat.from(context!!)) {
            notify(notificationId++, mBuilder.build())
        }
    }

    /* TODO: static messages

    case ROOM_CREATED:
    case ROOM_CREATE_TOO_MANY_ROOMS:
    case ROOM_CREATE_NAME_TAKEN: {
        ((EventListener)this.conn.objects.remove(Client.ROOM_CREATE_CALLBACK_PREFIX  + msg.readShort())).handleEvent(new Event(this, ROOM_CREATE_CALLBACK_EVENT, (msgType == MsgTypesDown.ROOM_CREATED) ? null : (msgType == MsgTypesDown.ROOM_CREATE_NAME_TAKEN))); 	// 'Client.ROOM_CREATE_CALLBACK_PREFIX  = "c1:"'
        break;
    }

    case MESSAGES: {
        final ArrayList<Message> userMessages = new ArrayList<Message>();
        while (msg.available() > 0) {
            final long sendDate = msg.readLong();
            final String name = msg.readUTF();
            final User sender = new User(name, msg.readInt());
            userMessages.add(new Message(sendDate, sender, msg.readUTF()));
        }
        this.emit(MESSAGES_WAITING_EVENT, userMessages);
        break;
    }
    case MESSAGE_CREATE_SUCCESS: {
        this.leaveMessageResult(LEAVE_MESSAGE_SUCCESS_EVENT, msg);
        break;
    }
    case MESSAGE_CREATE_NO_USER: {
        this.leaveMessageResult(LEAVE_MESSAGE_NO_USER_EVENT, msg);
        break;
    }
    case MESSAGE_CREATE_FULL: {
        this.leaveMessageResult(LEAVE_MESSAGE_FULL_MAILBOX_EVENT, msg);
        break;
    }
    case MESSAGE_CREATE_CONNECTED: {
        this.leaveMessageResult(LEAVE_MESSAGE_DEST_LOGGED_IN_EVENT, msg);
        break;
    }

    case CHALLENGE_CREATED: {
        final short callbackKeyRcv = msg.readShort();
        final CChallenge chal = (CChallenge)this.readGame(msg);
        final EventListener listener = (EventListener) this.conn.objects.remove(CRoom.CHALLENGE_PREFIX + callbackKeyRcv);
        listener.handleEvent(new Event(this, CHALLENGE_CREATED_EVENT, chal));
        break;
    }
    case CHALLENGE_NOT_CREATED: {
        final short callbackKeyRcv = msg.readShort();
        final EventListener listener = (EventListener) this.conn.objects.remove(CRoom.CHALLENGE_PREFIX + callbackKeyRcv);
        listener.handleEvent(new Event(this, CHALLENGE_NOT_CREATED_EVENT,
                (msg.available() > 0) ? msg.readLong() : null));
        break;
    }
    case SERVER_STATS: {
        if (this.ssListener != null) {
            this.ssListener.handleEvent(new Event(this, SERVER_STATS_EVENT, new ServerStats(msg, System.currentTimeMillis() - this.ssSendTime)));
            this.ssListener = null;
            break;
        }
        break;
    }

    case REGISTER_SUCCESS: {
        this.emit(REGISTER_SUCCESS_EVENT);
        break;
    }
    case REGISTER_BAD_EMAIL: {
        this.emit(REGISTER_BAD_EMAIL_EVENT, msg.readUTF());
        break;
    }
    case DELETE_ACCOUNT_ALREADY_GONE: {
        this.emit(12, Defs.getString(ClientRes.DEL_ACCT_ALREADY_GONE, msg.readUTF())); 	// 'ClientRes.DEL_ACCT_ALREADY_GONE = 2031923641'
        break;
    }
    case DELETE_ACCOUNT_SUCCESS: {
        this.emit(12, Defs.getString(ClientRes.DEL_ACCT_SUCCESS, msg.readUTF())); 	// 'ClientRes.DEL_ACCT_SUCCESS = 2031923669'
        break;
    }
    case SUBSCRIPTION_LOW: {
        final int days = msg.readByte() + 1;
        this.emit(13, Defs.getString(ClientRes.SUBSCRIPTION_LOW, new Object[] { days, Config.getWEB_HOST(), Plural.getCategory(days) })); 	// 'ClientRes.SUBSCRIPTION_LOW = 2031923635' 	// 'Config.WEB_HOST  = "webHost"'
        break;
    }

    case IDLE_WARNING: {
        this.emit(LONG_IDLE_WARNING_EVENT);
        break;
    }
    case CANT_PLAY_TWICE: {
        final EventListener listener2 = (EventListener) this.conn.objects.remove(CRoom.CHALLENGE_PREFIX + msg.readShort());
        if (listener2 == null) {
            this.emit(CANT_PLAY_TWICE_EVENT);
        }
        else {
            listener2.handleEvent(new Event(this, CANT_PLAY_TWICE_EVENT));
        }
        break;
    }
    case LOAD_FAILED: {
        this.emit(14, Defs.getString(ClientRes.BAD_GAME)); 	// 'ClientRes.BAD_GAME = 2031923645'
        break;
    }
    case GAME_STARTED: {
        this.emit(GAME_LOADED_EVENT, this.loadGameSummary(msg));
        break;
    }

    case ARCHIVE_JOIN:
    case TAG_ARCHIVE_JOIN: {
        final CArchive arc = this.loadArchive(msg, msgType == MsgTypesDown.TAG_ARCHIVE_JOIN);
        this.conn.objects.put(arc.id, arc);
        this.conn.objects.put(arc.getClassPrefix() + arc.owner.canonName(), arc);
        break;
    }
    case ARCHIVE_NONEXISTANT:
    case TAG_ARCHIVE_NONEXISTANT: {
        final EventListener callback = (EventListener) this.conn.objects.remove(((msgType == MsgTypesDown.ARCHIVE_NONEXISTANT) ? CArchive.ARCHIVE_CALLBACK_PREFIX  : CArchive.TAG_ARCHIVE_CALLBACK_PREFIX ) + msg.readUTF()); 	// 'CArchive.ARCHIVE_CALLBACK_PREFIX  = "F2a:"', 'CArchive.TAG_ARCHIVE_CALLBACK_PREFIX  = "E-a:"'
        if (callback != null) {
            callback.handleEvent(new Event(this, 89));
        }
        break;
    }

    case AVATAR: {
        final String name2 = User.canonName(msg.readUTF());
        final EventListener listener3 = (EventListener) this.conn.objects.remove("pic:" + name2);
        byte[] data = null;
        if (msg.available() > 0) {
            data = new byte[msg.available()];
            msg.readFully(data);
        }
        listener3.handleEvent(new Event(this, AVATAR_DATA_EVENT, data));
        break;
    }
    case SYNC: {
        this.conn.performSyncCallback(msg.readShort());
        break;
    }
    case GAME_NOTIFY: {
        this.readGame(msg);
        break;
    }
    case GAME_REVIEW: {
        final CGame original = (CGame) this.conn.objects.get(msg.readInt());
        final CGame review = (CGame)this.readGame(msg);
        if (original == null) {
            System.err.println("Can't find parent, but told of review!");
        }
        else {
            review.setOriginal(original);
        }
        break;
    }
    case PLAYBACK_ADD: {
        while (msg.available() > 0) {
            final long pbId = msg.readLong();
            final boolean subsOnly = msg.readBoolean();
            this.playbackList.put(pbId, new PlaybackInfo(pbId, subsOnly, this.loadGameSummary(msg)));
        }
        this.emit(PLAYBACK_LIST_CHANGED_EVENT, this.playbackList);
        break;
    }
    case PLAYBACK_DELETE: {
        while (msg.available() > 0) {
            this.playbackList.remove(msg.readLong());
        }
        this.emit(PLAYBACK_LIST_CHANGED_EVENT, this.playbackList);
        break;
    }
    case ALREADY_IN_PLAYBACK: {
        this.emit(13, Defs.getString(ClientRes.ALREADY_IN_PLAYBACK)); 	// 'ClientRes.ALREADY_IN_PLAYBACK = 2031923638'
        break;
    }
    case PLAYBACK_ERROR: {
        this.emit(13, Defs.getString(ClientRes.PLAYBACK_ERROR)); 	// 'ClientRes.PLAYBACK_ERROR = 2031923634'
        break;
    }
    case PLAYBACK_SETUP: {
        final int chanId = msg.readInt();
        final long len = msg.readLong();
        this.emit(PLAYBACK_CREATED_EVENT, this.buildPlayback(chanId, len, this.loadGameSummary(msg)));
        break;
    }
    case GLOBAL_GAMES_JOIN: {
        this.emit(GLOBAL_GAME_LIST_JOINED_EVENT, new CGlobalGames(this.conn, msg.readInt(), this, msg));
        break;
    }
    case PRIVATE_KEEP_OUT:
    case CHANNEL_SUBSCRIBERS_ONLY: {
        this.emit((msgType == MsgTypesDown.PRIVATE_KEEP_OUT) ? PRIVATE_KEEP_OUT_EVENT : SUBSCRIBERS_ONLY_EVENT, this.conn.objects.get(msg.readInt()));
        break;
    }
    case TOURN_NOTIFY: {
        final int gameId = msg.readInt();
        final int roomId = msg.readInt();
        new TournNotifyHandler((CRoom)this.conn.objects.get(roomId), (CGameListEntry)this.conn.objects.get(gameId), msg.readBoolean());
        break;
    }
    case AUTOMATCH_STATUS: {
        this.automatchRunning = msg.readBoolean();
        this.emit(AUTOMATCH_STATUS_CHANGED_EVENT);
        break;
    }
    case AUTOMATCH_PREFS: {
        final int newPrefs = msg.readInt();
        if (newPrefs != this.automatchPrefs) {
            this.automatchPrefs = newPrefs;
            this.emit(AUTOMATCH_PREFS_EVENT);
        }
        break;
    }
    case FETCH_TAGS_RESULT: {
        this.myTags = new LongHashMap<String>();
        while (msg.available() > 0) {
            final long gameId2 = msg.readLong();
            this.myTags.put(gameId2, msg.readUTF());
        }
        this.emit(FETCH_TAGS_SUCCESS_EVENT);
        break;
    }
    */

    /* channel based events

     */

    private fun handleConnectionEvent(ev: ConnectionEvent) {
        when (ev.msg) {
            "MESSAGE_IN_EVENT" -> {
                val buf = ev.arg as ByteArray
                val inputStream = DataInputStream(ByteArrayInputStream(buf))

                val msgType = MsgTypesDown.idToMsgType(inputStream.readShort().toInt())
                        ?: MsgTypesDown.UNKNOWN

                try {
                    msgType.emit(inputStream)
                } catch (ex: Exception) {
                    val writer = StringWriter()
                    val printWriter = PrintWriter(writer)
                    ex.printStackTrace(printWriter)
                    printWriter.flush()

                    Log.d(TAG, "${msgType.name} handler error:\n$writer")
                }
            }

            "MESSAGE_OUT_EVENT" ->
                Log.d(TAG, "${ev.msg}: ${ev.arg}")

            "OPEN_EVENT" -> {
                Log.d(TAG, "${ev.msg}: ${ev.arg}")
                connected = ev.connected
                lastConnectionMsg = ev.msg
                ConnectionStatus(connected, ev.msg).emit()

                // normal rooms get connected with subscribe events
                convos.eachChannel { channel ->
                    channel.connectionStatus(ev.connected)
                }
            }

            "CLOSED_EVENT" -> {
                Log.d(TAG, "${ev.msg}: ${ev.arg}")
                connected = ev.connected
                lastConnectionMsg = ev.msg
                ConnectionStatus(ev.connected, ev.msg).emit()
                loggedIn = ev.connected
                lastLoginMsg = "Logged out"
                LoginStatus(ev.connected, lastLoginMsg).emit()

                rooms.eachChannel { channel ->
                    channel.connectionStatus(ev.connected)
                }
                convos.eachChannel { channel ->
                    channel.connectionStatus(ev.connected)
                }
            }

            else -> throw Exception("Unknown ConnectionEvent type: ${ev.msg}")
        }
    }
}

class ConvoListEvent(val channelID: Int?) {
    companion object : Event<ConvoListEvent>()

    fun emit() = emit(this)
}

data class ConnectionStatus(val connected: Boolean, val msg: String) {
    companion object : Event<ConnectionStatus>()

    fun emit() = emit(this)
}

data class LoginStatus(val loggedIn: Boolean, val msg: String) {
    companion object : Event<LoginStatus>()

    fun emit() = emit(this)
}

data class ErrorEvent(val msg: String) {
    companion object : Event<ErrorEvent>()

    fun emit() = emit(this)
}
