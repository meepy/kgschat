package com.randomdrop.kgs.kgschat.model

import com.randomdrop.kgs.kgschat.Event
import java.io.DataInputStream
import java.util.*

class Room(channelID: Int, name: String, var flags: Short, var category: RoomList.CategoryType? = null) : Channel(channelID, name) {

    val challenges = Challenges()
    val games = Games()

    var description = ""
        @Synchronized set(value) {
            val re = Regex("\n{2,}")
            val newDesc = "<p>" + re.replace(value.trim(), "</p><br><p>")
                            .replace("\n", "</p><p>") + "</p><br>"
            if (field != newDesc) {
                field = newDesc
                RoomChatEvent(channelID, newDesc, lastMsgID).emit()
            }
        }
        @Synchronized get

    val owners = HashMap<String, User>()
    val users = HashMap<String, User>()

    @Synchronized
    override fun addMessage(user: User?, content: String, isAnnounce: Boolean,
                   isMe: Boolean, isCounted: Boolean): String? {
        val msg = super.addMessage(user, content, isAnnounce, isMe, isCounted)

        if (msg != null)
            RoomChatEvent(channelID, msg, lastMsgID).emit()

        return msg
    }

    @Synchronized
    override fun getMessages(processMsg: (String) -> Unit): Int {
        if (!description.isEmpty())
            processMsg(description)
        for (msg in messages)
            processMsg(msg)
        return lastMsgID
    }

    @Synchronized
    fun readGamesAndChallenges(inputStream: DataInputStream) {
        games.clear()
        challenges.clear()
        users.clear()

        inputStream.readByte() // wat?
        loop@ while (inputStream.available() > 0) {
            val typeID = inputStream.readByte().toInt()

            when (typeID) {
                -1 -> break@loop
                0 -> challenges.readChallenge(inputStream)
                else -> games.readGame(typeID, inputStream)
            }
        }

        while (inputStream.available() > 0) {
            val user = UserList.getUser(inputStream) ?: break
            users[user.name] = user
        }

        RoomUsersChangedEvent(this).emit()
    }

    @Synchronized
    fun readDescriptionAndOwners(inputStream: DataInputStream) {
        inputStream.readByte() // wat??
        description = inputStream.readUTF()
        owners.clear()
        while (inputStream.available() > 0) {
            val user = UserList.getUser(inputStream)
            if (user != null)
                owners[user.name] = user
        }
    }

    fun addUser(inputStream: DataInputStream) {
        val user = UserList.getUser(inputStream) ?: return
        users[user.name] = user

        RoomUsersChangedEvent(this).emit()
    }

    fun removeUser(inputStream: DataInputStream) {
        val user = UserList.getUser(inputStream) ?: return
        users.remove(user.name)

        RoomUsersChangedEvent(this).emit()
    }
}

data class RoomUsersChangedEvent(val room: Channel) {
    companion object : Event<RoomUsersChangedEvent>()

    fun emit() = emit(this)
}

