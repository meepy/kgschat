package com.randomdrop.kgs.kgschat

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle

class ClearChatDialog : DialogFragment() {
    interface ClearChatListener {
        fun onDialogOKClick(dialog: DialogFragment)
    }

    var selectedClearNew = true
    var selectedClearAll = false

    private lateinit var mListener: ClearChatListener

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        try {
            mListener = activity as ClearChatListener
        } catch (e: ClassCastException) {
            throw ClassCastException((activity.toString() +
                    " must implement ClearChatListener"))
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val mSelectedItems = ArrayList<Int>() // Where we track the selected items

            val checkedItems = arrayOf(true, false)

            for ((index, value) in checkedItems.withIndex()) {
                if (value)
                    mSelectedItems.add(index)
            }

            val builder = AlertDialog.Builder(it)
            builder.setSingleChoiceItems(R.array.clear_choices, 0) { dialog, id ->
                    selectedClearNew = (id == 0)
                    selectedClearAll = (id == 1)
                }
                .setPositiveButton(R.string.ok) { dialog, id ->
                    mListener.onDialogOKClick(this)
                }
                .setNegativeButton(R.string.cancel) { dialog, id ->
                }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}