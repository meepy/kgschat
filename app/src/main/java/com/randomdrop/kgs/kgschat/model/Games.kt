package com.randomdrop.kgs.kgschat.model

import java.io.DataInputStream


class Games {
    // TODO: keep list of current games

    fun clear() {

    }

    fun readGame(typeID: Int, inputStream: DataInputStream) {

        val gameID = inputStream.readInt() // TODO: update current game

        val roomID = inputStream.readInt()
        val roles = Roles(inputStream)

        val numObservers = inputStream.readShort()
        val size = inputStream.readByte().toInt()
        val hcap = inputStream.readByte().toInt()
        val komi2 = inputStream.readByte().toInt()
        val moveNum = inputStream.readShort().toInt()
    }
}