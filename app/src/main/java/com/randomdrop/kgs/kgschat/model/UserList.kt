package com.randomdrop.kgs.kgschat.model

import android.util.Log
import android.util.SparseArray
import com.randomdrop.kgs.kgschat.Event
import com.randomdrop.kgs.kgschat.MainActivity.Companion.TAG
import com.randomdrop.kgs.kgschat.MsgTypesDown
import java.io.DataInputStream
import java.util.*

class UserList {
    companion object {
        private val users: HashMap<String, User> = HashMap()

        fun getUser(inputStream: DataInputStream): User? {
            val name: String? = try {
                inputStream.readUTF()
            } catch (ex: Exception) { // TODO: determine why this happens
                Log.d(TAG, "getUser exception")
                null
            }

            if (name == null || name.isEmpty())
                return null

            val flags = inputStream.readInt()

            var user = users[name]
            if (user == null) {
                user = User(name, flags)
                users[name] = user
                UserStatusEvent(name).emit()
            } else if (flags != user.flags) {
                user.flags = flags
                UserStatusEvent(name).emit()
            }

            return user
        }
    }

    val buddied = FriendGroup(FriendGroupTypes.BUDDY)
    val censored = FriendGroup(FriendGroupTypes.CENSOR)
    val fanned = FriendGroup(FriendGroupTypes.FAN)
    val adminwatched = FriendGroup(FriendGroupTypes.ADMINWATCH)

    // channels for user details
    val details = SparseArray<UserDetails>()

    fun startListening() {
        // static messages
        MsgTypesDown.DETAILS_JOIN.on(this::processMsgDetailsJoin)
        MsgTypesDown.DETAILS_NONEXISTANT.on(this::processMsgDetailsNonexistent)

        // targeted messages
        MsgTypesDown.USER_UPDATE.on(this::processMsgUserUpdate)
        MsgTypesDown.DETAILS_UPDATE.on(this::processMsgDetailsUpdate)
        MsgTypesDown.DETAILS_RANK_GRAPH.on(this::processMsgDetailsRankGraph)
    }

    fun stopListening() {
        // static messages
        MsgTypesDown.DETAILS_JOIN.off(this::processMsgDetailsJoin)
        MsgTypesDown.DETAILS_NONEXISTANT.off(this::processMsgDetailsNonexistent)

        // targeted messages
        MsgTypesDown.USER_UPDATE.off(this::processMsgUserUpdate)
        MsgTypesDown.DETAILS_UPDATE.off(this::processMsgDetailsUpdate)
        MsgTypesDown.DETAILS_RANK_GRAPH.off(this::processMsgDetailsRankGraph)
    }

    private fun processMsgUserUpdate(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        getUser(inputStream)
    }

    private fun processMsgDetailsJoin(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val id = inputStream.readInt()
        val user = UserList.getUser(inputStream) ?:
            throw Exception("User details received with no User")
        val userDetails = details.get(id) ?: UserDetails(id, user)
        details.put(id, userDetails)
        userDetails.read(inputStream)
    }

    private fun processMsgDetailsUpdate(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val userDetails = details.get(channelID) ?:
            throw Exception("Attempt to update unknown user details")
        userDetails.read(inputStream)
    }

    private fun processMsgDetailsNonexistent(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val name = inputStream.readUTF()
        ErrorEvent("Non-existent user: $name").emit()
    }

    private fun processMsgDetailsRankGraph(msgType: MsgTypesDown, channelID: Int, inputStream: DataInputStream) {
        val userDetails = details.get(channelID)
            ?: throw Exception("Attempt to read rank graph for unknown user details")
        userDetails.readRankGraph(inputStream)
    }
}

data class UserStatusEvent(val name: String) {
    companion object : Event<UserStatusEvent>()
    fun emit() = emit(this)
}
