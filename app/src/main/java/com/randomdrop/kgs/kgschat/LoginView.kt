package com.randomdrop.kgs.kgschat

import android.content.Context
import android.content.Intent
import android.view.inputmethod.InputMethodManager
import com.randomdrop.kgs.kgschat.model.ConnectionStatus
import com.randomdrop.kgs.kgschat.model.LoginStatus
import com.randomdrop.kgs.kgschat.model.Model
import kotlinx.android.synthetic.main.login.*


class LoginView(val activity: MainActivity) {

    private lateinit var model: Model

    private var commIntent : Intent? = null // current LOGIN Intent

    fun start(model: Model) {
        this.model = model
    }

    fun stop() {
        commIntent?.let { activity.stopService(it) }
    }

    private fun processConnectionStatus(ev: ConnectionStatus) {
        activity.runOnUiThread {
            activity.connectionStatus.text = (if (ev.connected) "Connection: on" else "Connection: off") +
                    '\n' + ev.msg

            if (ev.connected) {
                activity.button.text = activity.getString(R.string.button_logout)
                activity.button.isEnabled = true
                activity.editText1.isEnabled = false
                activity.editText2.isEnabled = false
                activity.checkBox.isEnabled = false
                activity.button.setOnClickListener(activity::logout)
            } else {
                activity.button.text = activity.getString(R.string.button_login)
                activity.button.isEnabled = true
                activity.editText1.isEnabled = true
                activity.editText2.isEnabled = !activity.checkBox.isSelected
                activity.checkBox.isEnabled = true
                activity.button.setOnClickListener(activity::loginRegular)
            }
        }
    }

    private fun processLoginStatus(ev: LoginStatus) {
        activity.runOnUiThread {
            activity.loginStatus2.text = (if (ev.loggedIn) "Login: on" else "Login: off") +
                    '\n' + ev.msg
        }
    }

    fun login() {
        commIntent = Intent().also {
            it.setClassName("com.randomdrop.kgs.kgschat",
                    "com.randomdrop.kgs.kgschat.KGSSocketService")
            it.action = KGSSocketService.ACTION_LOGIN
            model.setUserPassword(activity.editText1.text.toString(),
                    if (activity.editText2.isEnabled) activity.editText2.text.toString() else "")
        }

        activity.startService(commIntent)

        clearLoginFocus()
    }

    private fun clearLoginFocus() {
        activity.button.isEnabled = false
        activity.editText1.clearFocus()
        activity.editText2.clearFocus()
        activity.editText1.isEnabled = false
        activity.editText2.isEnabled = false
        activity.checkBox.isEnabled = false
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(activity.editText1.windowToken, 0)
    }

    fun logout() {
        model.logout()
        clearLoginFocus()
    }

    fun guest() {
        activity.editText2.isEnabled = !activity.checkBox.isChecked
        if (!activity.editText2.isEnabled) {
            activity.editText2.setText("")
        }
    }

    fun startListening() {
        processConnectionStatus(ConnectionStatus(model.connected, model.lastConnectionMsg))
        ConnectionStatus.on(this::processConnectionStatus)

        processLoginStatus(LoginStatus(model.loggedIn, model.lastLoginMsg))
        LoginStatus.on(this::processLoginStatus)
    }

    fun stopListening() {
        ConnectionStatus.off(this::processConnectionStatus)
        LoginStatus.off(this::processLoginStatus)
    }
}