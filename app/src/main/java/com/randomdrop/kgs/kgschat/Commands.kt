package com.randomdrop.kgs.kgschat

import android.content.Intent
import android.os.Bundle
import android.util.SparseArray
import com.randomdrop.kgs.kgschat.RsaCrypto.passwordCompute
import com.randomdrop.kgs.kgschat.model.FriendGroupTypes
import java.security.AccessControlException
import java.util.*

/*
    static commands for talking to the server
 */
object Commands {
    const val CHAT_MAX_LEN = 1000

    const val VERSION_BUGFIX = 22
    const val VERSION_BETA = false // string value for real beta
    const val PASSWORD_LEN = 256

    fun send(tx: TxMessage) {
        val msg = Bundle()
        msg.putByteArray("tx", tx.toByteArray())

        val commIntent = Intent().also {
            it.setClassName("com.randomdrop.kgs.kgschat",
                    "com.randomdrop.kgs.kgschat.KGSSocketService")
            it.action = KGSSocketService.ACTION_SEND
            it.putExtras(msg)
        }

        MainActivity.context!!.startService(commIntent)
    }

    fun close() {
        val commIntent = Intent().also {
            it.setClassName("com.randomdrop.kgs.kgschat",
                    "com.randomdrop.kgs.kgschat.KGSSocketService")
            it.action = KGSSocketService.ACTION_CLOSE
        }

        MainActivity.context!!.startService(commIntent)
    }

    fun sendChat(channelID: Int, msg: String, isAnnounce: Boolean, toAll: Boolean) {
        if (msg.isEmpty())
            return

        val msgType = if (isAnnounce) {
            if (toAll)
                MsgTypesUp.ANNOUNCE_TO_PLAYERS
            else
                MsgTypesUp.ANNOUNCE
        } else
            MsgTypesUp.CHAT

        val tx = TxMessage(msgType)
        tx.writeInt(channelID)

        if (msg.length > CHAT_MAX_LEN)
            tx.writeUTF(msg.substring(0, CHAT_MAX_LEN).replace('\n', ' '))
        else
            tx.writeUTF(msg.replace('\n', ' '))

        send(tx)
    }

    val origName = SparseArray<String>()
    private var callBackKey = 1 // TODO: use this for CONVO_JOIN / CONVO_NO_SUCH_USER

    fun convoRequest(name: String) {
        val tx = TxMessage(MsgTypesUp.CONVO_REQUEST)
        origName.put(callBackKey, name)
        tx.writeShort(callBackKey++)
        tx.writeUTF(name.toLowerCase(Locale.ENGLISH))
        send(tx)
    }

    fun sendLogin(passwordXor: Long, clientType: ClientType, clientId: Long, userName: String, password: String) {
        val tx = TxMessage(MsgTypesUp.LOGIN)
        tx.writeShort(clientType.ordinal)
        tx.writeShort(VERSION_BUGFIX)

        val beta: Int = if (VERSION_BETA) 1 else -1
        tx.writeShort(beta)

        tx.writeUTF("en_US") //Defs.getString(ClientRes.LOCALE))
        tx.writeLong(clientId)
        tx.writeUTF(userName)

        if (password.isEmpty()) {
            tx.writeBoolean(false)
        } else {
            tx.writeBoolean(true)
            tx.write(RsaCrypto.rsaEncrypt(passwordCompute(password) xor passwordXor, PASSWORD_LEN),
                    0, PASSWORD_LEN)
        }

        val tokens = StringTokenizer("java.version java.vendor os.name os.arch os.version")
        while (tokens.hasMoreElements()) {
            var prop: String? = null
            try {
                prop = System.getProperty(tokens.nextToken())
            } catch (e: AccessControlException) {
            }
            tx.writeUTF(prop ?: "")
        }

        send(tx)
    }

    fun addFriend(groupType: FriendGroupTypes, name: String, note: String) {
        val tx = TxMessage(MsgTypesUp.FRIEND_ADD)
        origName.put(callBackKey, name)
        tx.writeShort(callBackKey++)
        tx.write(groupType.id)
        tx.writeUTF(name.toLowerCase(Locale.ENGLISH))
        tx.writeUTF(note)
        send(tx)
    }

    fun removeFriend(groupType: Int, name: String) {
        val tx = TxMessage(MsgTypesUp.FRIEND_REMOVE)
        origName.put(callBackKey, name)
        tx.writeShort(callBackKey++)
        tx.write(groupType)
        tx.writeUTF(name.toLowerCase(Locale.ENGLISH))
        send(tx)
    }

    fun requestDetails(name: String) {
        val tx = TxMessage(MsgTypesUp.DETAILS_JOIN_REQUEST)
        tx.writeUTF(name.toLowerCase(Locale.ENGLISH))
        send(tx)
    }

    fun requestRankGraph(channelID: Int) {
        val tx = TxMessage(MsgTypesUp.DETAILS_RANK_GRAPH_REQUEST)
        tx.writeInt(channelID)
        send(tx)
    }
}
