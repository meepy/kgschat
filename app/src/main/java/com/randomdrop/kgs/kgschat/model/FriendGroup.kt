package com.randomdrop.kgs.kgschat.model

import android.util.Log
import com.randomdrop.kgs.kgschat.MainActivity
import java.util.*

enum class FriendGroupTypes(val id: Int) {
    BUDDY(0), CENSOR(1), FAN(2), ADMINWATCH(3);
}

class FriendGroup(val type: FriendGroupTypes) : HashSet<User>() {
    override fun add(element: User): Boolean {
        Log.d(MainActivity.TAG, "FriendGroup ${type.name} add: ${element.name}")
        return super.add(element)
    }

    override fun remove(element: User): Boolean {
        Log.d(MainActivity.TAG, "FriendGroup ${type.name} remove: ${element.name}")
        return super.remove(element)
    }

    fun toNameList(): List<String> {
        return  map { user ->
            user.name
        }
    }
}