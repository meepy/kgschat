package com.randomdrop.kgs.kgschat

import android.content.SharedPreferences


class Prefs {

    lateinit var sPrefs: SharedPreferences

    var mobileClient: Boolean = true
        set(value) {
            with(sPrefs.edit()) {
                putBoolean("mobileClient", value)
                apply()
            }
            field = value
        }

    var highlightNewMessages: Boolean = true
        set(value) {
            with(sPrefs.edit()) {
                putBoolean("highlightMessages", value)
                apply()
            }
            field = value
        }

    var buzzNotification: Boolean = false
        set(value) {
            with(sPrefs.edit()) {
                putBoolean("buzzNotification", value)
                apply()
            }
            field = value
        }

    var soundNotification: Boolean = false
        set(value) {
            with(sPrefs.edit()) {
                putBoolean("soundNotification", value)
                apply()
            }
            field = value
        }

    fun readDefaults(prefs: SharedPreferences) {
        sPrefs = prefs

        mobileClient = prefs.getBoolean("mobileClient", true)
        highlightNewMessages = prefs.getBoolean("highlightNewMessages", true)
        buzzNotification = prefs.getBoolean("buzzNotification", false)
        soundNotification = prefs.getBoolean("soundNotification", false)
    }
}