package com.randomdrop.kgs.kgschat

import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.os.Bundle

class SettingsDialog : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val mSelectedItems = ArrayList<Int>() // Where we track the selected items

            val checkedItems = arrayOf(
                    MainActivity.prefs.mobileClient,
                    MainActivity.prefs.highlightNewMessages,
                    MainActivity.prefs.buzzNotification,
                    MainActivity.prefs.soundNotification)

            for ((index, value) in checkedItems.withIndex()) {
                if (value)
                    mSelectedItems.add(index)
            }

            val builder = AlertDialog.Builder(it)
            builder.setTitle(R.string.action_settings)
                .setMultiChoiceItems(R.array.setting_choices,
                        checkedItems.toBooleanArray()) { dialog, which, isChecked ->
                    if (isChecked) {
                        mSelectedItems.add(which)
                    } else {
                        mSelectedItems.remove(Integer.valueOf(which))
                    }
                }
                .setPositiveButton(R.string.ok) { dialog, id ->
                    MainActivity.prefs.mobileClient = mSelectedItems.contains(0)
                    MainActivity.prefs.highlightNewMessages = mSelectedItems.contains(1)
                    MainActivity.prefs.buzzNotification = mSelectedItems.contains(2)
                    MainActivity.prefs.soundNotification = mSelectedItems.contains(3)
                }
                .setNegativeButton(R.string.cancel) { dialog, id ->
                }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}