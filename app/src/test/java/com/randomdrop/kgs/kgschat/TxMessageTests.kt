package com.randomdrop.kgs.kgschat

import org.junit.Assert
import org.junit.Test

import org.junit.Before
import java.util.*

/**
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TxMessageTests {
    var oldMsg: TxMessageOld = TxMessageOld()
    var newMsg : TxMessage = TxMessage()

    @Before
    fun beforeEachTest() {
        oldMsg = TxMessageOld()
        newMsg = TxMessage()
    }

    @Test
    fun writeBoolean() {
        oldMsg.writeBoolean(true)
        newMsg.writeBoolean(true)

        Assert.assertArrayEquals(oldMsg.bytes, newMsg.getBytes())
    }

    @Test
    fun writeUTF() {
        oldMsg.writeUTF("i am a teapot short and stout")
        newMsg.writeUTF("i am a teapot short and stout")

        System.out.println(Arrays.toString(oldMsg.bytes))
        System.out.println(Arrays.toString(newMsg.getBytes()))
        System.out.flush()
        Assert.assertArrayEquals(oldMsg.bytes, newMsg.getBytes())
    }

    @Test
    fun writeLong() {
        oldMsg.writeLong(12345678)
        newMsg.writeLong(12345678)
        Assert.assertArrayEquals(oldMsg.bytes, newMsg.getBytes())
    }

    @Test
    fun writeWithIntArg() {
        oldMsg.write(500)
        newMsg.write(500)
        Assert.assertArrayEquals(oldMsg.bytes, newMsg.getBytes())
    }

    @Test
    fun writeByte() {
        oldMsg.writeByte(500)
        newMsg.writeByte(500)
        Assert.assertArrayEquals(oldMsg.bytes, newMsg.getBytes())
    }

    @Test
    fun writeBytes() {
        oldMsg.writeBytes("Moisture is the essence of wetness, and wetness is the essence of beauty.")
        newMsg.writeBytes("Moisture is the essence of wetness, and wetness is the essence of beauty.")
        Assert.assertArrayEquals(oldMsg.bytes, newMsg.getBytes())
    }

    @Test
    fun writeChar() {
        oldMsg.writeChar(500)
        newMsg.writeChar(500)
        Assert.assertArrayEquals(oldMsg.bytes, newMsg.getBytes())
    }

    @Test
    fun writeChars() {
        oldMsg.writeChars("I'm pretty sure there's a lot more to life than being really, really good looking. And I plan on finding out what that is.")
        newMsg.writeChars("I'm pretty sure there's a lot more to life than being really, really good looking. And I plan on finding out what that is.")
        Assert.assertArrayEquals(oldMsg.bytes, newMsg.getBytes())
    }

    @Test
    fun writeDouble() {
        oldMsg.writeDouble(500.25)
        newMsg.writeDouble(500.25)
        Assert.assertArrayEquals(oldMsg.bytes, newMsg.getBytes())
    }

    @Test
    fun writeFloat() {
        oldMsg.writeFloat(500.25f)
        newMsg.writeFloat(500.25f)
        Assert.assertArrayEquals(oldMsg.bytes, newMsg.getBytes())
    }

    @Test
    fun writeInt() {
        oldMsg.writeInt(500)
        newMsg.writeInt(500)
        Assert.assertArrayEquals(oldMsg.bytes, newMsg.getBytes())
    }

    @Test
    fun writeShort() {
        oldMsg.writeShort(500)
        newMsg.writeShort(500)
        Assert.assertArrayEquals(oldMsg.bytes, newMsg.getBytes())
    }
}
